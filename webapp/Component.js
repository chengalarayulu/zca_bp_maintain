sap.ui.define([
	"sap/ui/core/UIComponent",
	"sap/ui/Device",
	"ZCA/ZCA_BP_MAINTAIN/model/models",
	"ZCA/ZCA_BP_MAINTAIN/controller/Utility"
], function (UIComponent, Device, models, Utility) {
	"use strict";

	return UIComponent.extend("ZCA.ZCA_BP_MAINTAIN.Component", {

		metadata: {
			manifest: "json"
		},

		/**
		 * The component is initialized by UI5 automatically during the startup of the app and calls the init method once.
		 * @public
		 * @override
		 */
		init: function () {

			// Set logged in user
			Utility.setUserApiModel();
			//
			var vUrl = "https://services.odata.org/Northwind/Northwind.svc";
			var oModel = new sap.ui.model.odata.v2.ODataModel({
				serviceUrl: vUrl
					// serviceUrlParams: {
					// 	myParam: "value1",
					// 	myParam2: "value2"
					// },
					// metadataUrlParams: {
					// 	myParam: "value1",
					// 	myParam2: "value2"
					// }
			});

			oModel.read("/Customers", {
				success: function (oData, oResponse) {

				},
				error: function (oEvent) {

				}
			}); // End of A_Details

			//
			/*// will work only for S4 deployed applicaitons */
			// var oUserData;
			// var y = "/sap/bc/ui2/start_up";
			// var xmlHttp = null;
			// xmlHttp = new XMLHttpRequest();
			// xmlHttp.onreadystatechange = function () {
			// 	if (xmlHttp.readyState === 4 && xmlHttp.status === 200) {
			// 		oUserData = JSON.parse(xmlHttp.responseText);
			// 	}
			// };
			// xmlHttp.open("GET", y, false);
			// xmlHttp.send(null);

			// set the component instance 
			Utility._Component = this;

			// call the base component's init function
			UIComponent.prototype.init.apply(this, arguments);

			// enable routing
			this.getRouter().initialize();

			// set the device model
			this.setModel(models.createDeviceModel(), "device");

			this.getModel("vhCountry").setSizeLimit(Utility.vhSizeLimit);
			this.getModel("vhTaxCat").setSizeLimit(Utility.vhSizeLimit);
			this.getModel("vhSalesDist").setSizeLimit(Utility.vhSizeLimit);
			this.getModel("vhRegion").setSizeLimit(Utility.vhSizeLimit);
			this.getModel("vhCurrency").setSizeLimit(Utility.vhSizeLimit);

			// set default binding mode for uiModel 
			this.getModel("uiModel").setDefaultBindingMode("TwoWay");
			//
			this.getModel("comModel").setDefaultBindingMode("TwoWay");

			// Set login user role(Requester/Validator/Credit Control)
			Utility.setUserRole();
		},

		getContentDensityClass: function () {
			if (!this._sContentDensityClass) {
				if (!Device.support.touch) {
					this._sContentDensityClass = "sapUiSizeCompact";
				} else {
					this._sContentDensityClass = "sapUiSizeCozy";
				}
			}
			return this._sContentDensityClass;
		}

	});
});