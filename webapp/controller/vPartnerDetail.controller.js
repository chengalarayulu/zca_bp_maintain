sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"sap/m/MessageToast",
	"ZCA/ZCA_BP_MAINTAIN/controller/Utility"
], function (Controller, MToast, Utility) {
	"use strict";

	return Controller.extend("ZCA.ZCA_BP_MAINTAIN.controller.vPartnerDetail", {

		/**
		 * Called when a controller is instantiated and its View controls (if available) are already created.
		 * Can be used to modify the View before it is displayed, to bind event handlers and do other one-time initialization.
		 * @memberOf ZCA.ZCA_BP_MAINTAIN.view.vPartnerDetail
		 */
		onInit: function () {

		},

		onBack: function () {

			var lModel = this.getOwnerComponent().getModel("lModel");
			lModel.setProperty("/BP_Details", Utility._selectedRequestDetails);

			Utility._partnerType = Utility.keyNoPartner;
			Utility._partnerSeqNum = 0;

			Utility.setCommunicationSections();

			var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
			oRouter.navTo("vDetail");
		},

		onSave: function () {

			var lModel = this.getOwnerComponent().getModel("lModel");
			var btpDetails = lModel.getProperty("/BP_Details");
			var comModel = this.getOwnerComponent().getModel("comModel");
			//
			var comData = comModel.getProperty("/BP");

			switch (Utility._partnerAction) {
			case Utility.reqTypeNew:
				{
					// 
					btpDetails.RequestNumber = Utility._newRequest;
					btpDetails.PartnerType = Utility._partnerType;
					btpDetails.PartnerSeqNum = Utility._partnerSeqNum;

					btpDetails.PreferredCommType = Utility.getPreferredCommMethodPADisplay();

					lModel.getProperty("/Partners").push(btpDetails);

					// Emails
					var i = 0;
					for (; comData.Email[i];) {
						lModel.getProperty("/Communication").push(comData.Email[i]);
						i++;
					}
					// MobilePhone
					i = 0;
					for (; comData.MobilePhone[i];) {
						lModel.getProperty("/Communication").push(comData.MobilePhone[i]);
						i++;
					}
					// TelePhone
					i = 0;
					for (; comData.TelePhone[i];) {
						lModel.getProperty("/Communication").push(comData.TelePhone[i]);
						i++;
					}
					break;
				}
				// 
			case Utility.reqTypeUpdate:
				{
					lModel.setProperty(Utility._partnerPath, btpDetails);
					// var aComm = lModel.getProperty("/Communication");

					// 
					// Emails
					// for (i = 0; comData.Email[i]; i++) {

					// 	for (var j = 0; aComm[j]; j++) {
					// 		if (aComm[j].PartnerType === comData.Email[i].PartnerType && aComm[j].PartnerSeqNum === comData.Email[i].PartnerSeqNum && aComm[
					// 				j].CommType === comData.Email[i].CommType && aComm[j].CommSeqNumber === comData.Email[i].CommSeqNumber) {
					// 			aComm[j] = comData.Email[i];
					// 		}
					// 		// // New Comm created in change mode 
					// 		// else {
					// 		// 	// TODO: later
					// 		// lModel.getProperty("/Communication").push(comData.Email[i]);
					// 		// }
					// 	}
					// }
					// MobilePhone
					// for (i = 0; comData.MobilePhone[i]; i++) {
					// 	lModel.getProperty("/Communication").push(comData.MobilePhone[i]);
					// }
					// // TelePhone
					// for (i = 0; comData.TelePhone[i]; i++) {
					// 	lModel.getProperty("/Communication").push(comData.TelePhone[i]);
					// }

					Utility.setPreferredCommMethodPADisplay(btpDetails.PreferredCommType);
					break;
				}
			}

			//
			lModel.refresh();

			//
			// reset to main BP
			Utility._partnerType = Utility.keyNoPartner;
			Utility._partnerSeqNum = 0;

			Utility.setCommunicationSections();
			//
			lModel.setProperty("/BP_Details", Utility._selectedRequestDetails);
			MToast.show("Partner Saved");

			var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
			oRouter.navTo("vDetail");
		},

		// 
		// getPreferredCommMethodPA: function () {

		// 	var uiModel = this.getOwnerComponent().getModel("uiModel");
		// 	var mEntity = uiModel.getProperty("/");

		// 	// Email
		// 	if (mEntity.StateEmailPA === true) {
		// 		return Utility.keyDefaultEmail;
		// 	}
		// 	// MobilePhone
		// 	if (mEntity.StateMobPA === true) {
		// 		return Utility.keyMob;
		// 	}
		// 	// Telephone
		// 	if (mEntity.StateTelPA === true) {
		// 		return Utility.keyTel;
		// 	}
		// 	// MobilePhone
		// 	if (mEntity.StatePrintPA === true) {
		// 		return Utility.keyPrint;
		// 	}
		// },

		// // 
		// setPreferredCommMethodPA: function (sCommMethod) {

		// 	var uiModel = this.getOwnerComponent().getModel("uiModel");
		// 	var mEntity = uiModel.getProperty("/");

		// 	// Email
		// 	switch (sCommMethod) {
		// 	case Utility.keyDefaultEmail:
		// 		mEntity.StateEmailPA = true;
		// 		mEntity.StateMobPA = false;
		// 		break;
		// 	case Utility.keyMob:
		// 		mEntity.StateMobPA = true;
		// 		mEntity.StateEmailPA = false;
		// 		break;
		// 	case Utility.keyTel:
		// 		mEntity.StateTelPA = true;
		// 		mEntity.StateEmailPA = false;
		// 		mEntity.StateMobPA = false;
		// 		break;
		// 	case Utility.keyPrint:
		// 		mEntity.StateTelPA = false;
		// 		mEntity.StateEmailPA = false;
		// 		mEntity.StateMobPA = false;
		// 		mEntity.StatePrintPA = true;
		// 		break;
		// 	}
		// },

		/**
		 * Similar to onAfterRendering, but this hook is invoked before the controller's View is re-rendered
		 * (NOT before the first rendering! onInit() is used for that one!).
		 * @memberOf ZCA.ZCA_BP_MAINTAIN.view.vPartnerDetail
		 */
		onBeforeRendering: function () {
			Utility._vPartnerDetail = this;
		}

		/**
		 * Called when the View has been rendered (so its HTML is part of the document). Post-rendering manipulations of the HTML could be done here.
		 * This hook is the same one that SAPUI5 controls get after being rendered.
		 * @memberOf ZCA.ZCA_BP_MAINTAIN.view.vPartnerDetail
		 */
		// onAfterRendering: function() {
		// 
		// }

		/**
		 * Called when the Controller is destroyed. Use this one to free resources and finalize activities.
		 * @memberOf ZCA.ZCA_BP_MAINTAIN.view.vPartnerDetail
		 */
		//	onExit: function() {
		//
		//	}

	});

});