sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"sap/ui/model/Filter",
	"sap/ui/model/FilterOperator",
	"sap/m/MessageToast",
	"ZCA/ZCA_BP_MAINTAIN/controller/Utility"
], function (Controller, Filter, FilterOperator, MToast, Utility) {
	"use strict";

	return Controller.extend("ZCA.ZCA_BP_MAINTAIN.controller.vGeneralComm", {

		/**
		 * Called when a controller is instantiated and its View controls (if available) are already created.
		 * Can be used to modify the View before it is displayed, to bind event handlers and do other one-time initialization.
		 * @memberOf ZCA.ZCA_BP_MAINTAIN.view.vGeneralComm
		 */
		onInit: function () {
			// Utility.setViewUIbindings(Utility.keyNoPartner);
		},

		onChangeEmail: function (oEvent) {
			var oInput = oEvent.getSource();
			var sValue = oEvent.getParameter("newValue");
			if (!sValue || !(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(String(sValue.toLowerCase())))) {
				oInput.setValueState(sap.ui.core.ValueState.Error);
				oInput.setValueStateText("* Enter a valid email address");
			} else {
				oInput.setValueState(sap.ui.core.ValueState.None);
			}
			oInput.setValue(sValue.toLowerCase());
		},

		onPreferredEmail: function () {
			Utility.setPreferredCommMethod(false, Utility.keyEmail);
		},

		onPreferredTel: function () {
			Utility.setPreferredCommMethod(false, Utility.keyTel);
		},

		onPreferredMob: function () {
			Utility.setPreferredCommMethod(false, Utility.keyMob);
		},

		onPreferredPrint: function () {
			Utility.setPreferredCommMethod(false, Utility.keyPrint);
		},

		// 
		onPreferredEmailPA: function () {
			Utility.setPreferredCommMethod(true, Utility.keyEmail);
		},

		onPreferredTelPA: function () {
			Utility.setPreferredCommMethod(true, Utility.keyTel);
		},

		onPreferredMobPA: function () {
			Utility.setPreferredCommMethod(true, Utility.keyMob);
		},

		onPreferredPrintPA: function () {
			Utility.setPreferredCommMethod(true, Utility.keyPrint);
		},
		// 
		// // 
		// onPreferred: function (oEvent) {

		// 	var emailVisible = false;
		// 	var phoneVisible = false;
		// 	var teleVisible = false;
		// 	var faxVisible = false;

		// 	var oParams = oEvent.getParameters();

		// 	if (oParams.id.search("idsEmail") !== -1) {
		// 		emailVisible = true;
		// 	}
		// 	if (oParams.id.search("idsMobilePhone") !== -1) {
		// 		phoneVisible = true;
		// 	}
		// 	if (oParams.id.search("idsTelephone") !== -1) {
		// 		teleVisible = true;
		// 	}
		// 	if (oParams.id.search("idsFax") !== -1) {
		// 		faxVisible = true;
		// 	}

		// 	this.getView().byId("idsEmail").setState(emailVisible);
		// 	this.getView().byId("idsMobilePhone").setState(phoneVisible);
		// 	this.getView().byId("idsTelephone").setState(teleVisible);
		// 	this.getView().byId("idsFax").setState(faxVisible);
		// },

		// 
		onAddMobilePhone: function (oArgs) {
			this._addNewRow("idlMobilePhone", Utility.keyMob);
		},
		// 
		onDeleteMobilePhone: function (oArgs) {
			// TO DO
		},
		// 
		onAddTelephone: function (oArgs) {
			this._addNewRow("idlTelePhone", Utility.keyTel);
		},
		// 
		onDeleteTelephone: function (oArgs) {
			// TO DO
		},

		onAddEmail: function (oArgs) {
			this._addNewRow("idlEmail", Utility.keyEmail);
		},
		onDeleteEmail: function (oArgs) {
			// TO DO
		},

		_addNewRow: function (pId, pKey) {

			var bindingObject = this.getView().byId(pId);
			var oModel = bindingObject.getBinding("items").getModel();

			var newRows = {
				"PartnerType": Utility._partnerType,
				"PartnerSeqNum": Utility._partnerSeqNum,
				"CommType": pKey,
				"CommSeqNumber": bindingObject.getItems().length + 1
			};

			if (Utility.reqType === Utility.reqTypeNew) {
				newRows.RequestNumber = Utility._newRequest;
			} else {
				newRows.RequestNumber = Utility._selectedRequest.RequestNumber;
			}

			oModel.getProperty("/Communication").push(newRows);
			oModel.refresh();
		},

		// 
		onAddMobilePhonePA: function (oArgs) {
			this._addNewRowPA(oArgs, Utility.keyMob);
		},
		// 
		onDeleteMobilePhonePA: function (oArgs) {
			// TO DO
		},
		// 
		onAddTelephonePA: function (oArgs) {
			this._addNewRowPA(oArgs, Utility.keyTel);
		},
		// 
		onDeleteTelephonePA: function (oArgs) {
			// TO DO
		},

		onAddEmailPA: function (oArgs) {
			this._addNewRowPA(oArgs, Utility.keyEmail);
		},
		onDeleteEmailPA: function (oArgs) {
			// TO DO
		},

		_addNewRowPA: function (oArgs, pKey) {

			// var bindingObject = this.getView().byId(pId);
			var bindingObject = oArgs.getSource().getParent().getParent().getBinding("items");
			var oModel = bindingObject.getModel();

			var newRows = {
				"PartnerType": Utility._partnerType,
				"PartnerSeqNum": Utility._partnerSeqNum,
				"CommType": pKey,
				"CommSeqNumber": bindingObject.getLength() + 1,
				"DefaultComm": false
			};

			if (Utility.reqType === Utility.reqTypeNew) {
				newRows.RequestNumber = Utility._newRequest;
			} else {
				newRows.RequestNumber = Utility._selectedRequest.RequestNumber;
			}

			var vPath = "/" + Utility._partnerType;
			switch (pKey) {
			case Utility.keyEmail:
				vPath = vPath + "/Email";
				break;
			case Utility.keyMob:
				vPath = vPath + "/MobilePhone";
				break;
			case Utility.keyTel:
				vPath = vPath + "/TelePhone";
				break;
			}

			oModel.getProperty(vPath).push(newRows);
			oModel.refresh();
		},

		onPAemailDefault: function (oEvent) {
			var bindingContext = oEvent.getSource().getParent().getBindingContext("comModel");
			var oModel = bindingContext.getModel();
			var vPath = bindingContext.getPath();
			var len = vPath.length - 1;
			var index = parseInt(vPath.charAt(len), 10);

			var sPath = "/" + Utility._partnerType + "/Email";
			var cData = oModel.getProperty(sPath);

			var i = 0;
			for (; cData[i];) {
				if (i !== index) {
					cData[i].DefaultComm = false;
				}
				i++;
			}
			oModel.refresh();
		},
		// 
		onEmailDefault: function (oEvent) {
			var bindingContext = oEvent.getSource().getParent().getBindingContext("lModel");
			var oModel = bindingContext.getModel();
			var vPath = bindingContext.getPath();
			var len = vPath.length - 1;
			var index = parseInt(vPath.charAt(len), 10);

			var cData = oModel.getProperty("/Communication");

			var i = 0;
			for (; cData[i];) {
				if (i !== index && cData[i].PartnerType === Utility._partnerType && cData[i].PartnerSeqNum === Utility._partnerSeqNum &&
					cData[i].CommType === Utility.keyEmail) {
					cData[i].DefaultComm = false;
				}
				i++;
			}
			oModel.refresh();
		},

		//
		onSelectEmail: function (oEvent) {
			MToast.show("Selected");
		},

		/**
		 * Similar to onAfterRendering, but this hook is invoked before the controller's View is re-rendered
		 * (NOT before the first rendering! onInit() is used for that one!).
		 * @memberOf ZCA.ZCA_BP_MAINTAIN.view.vGeneralComm
		 */
		onBeforeRendering: function () {

			Utility._vGeneralComm = this;
			Utility._viewsLoaded = true;

			Utility.setViewUIbindings();

		}

		/**
		 * Called when the View has been rendered (so its HTML is part of the document). Post-rendering manipulations of the HTML could be done here.
		 * This hook is the same one that SAPUI5 controls get after being rendered.
		 * @memberOf ZCA.ZCA_BP_MAINTAIN.view.vGeneralComm
		 */
		// onAfterRendering: function () {
		// 	// Utility.setViewUIbindings();
		// }

		/**
		 * Called when the Controller is destroyed. Use this one to free resources and finalize activities.
		 * @memberOf ZCA.ZCA_BP_MAINTAIN.view.vGeneralComm
		 */
		//	onExit: function() {
		//
		//	}

	});

});