sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"ZCA/ZCA_BP_MAINTAIN/controller/Utility"
], function (Controller, Utility) {
	"use strict";

	return Controller.extend("ZCA.ZCA_BP_MAINTAIN.controller.vContact", {

		/**
		 * Called when a controller is instantiated and its View controls (if available) are already created.
		 * Can be used to modify the View before it is displayed, to bind event handlers and do other one-time initialization.
		 * @memberOf ZCA.ZCA_BP_MAINTAIN.view.vContact
		 */
		onInit: function () {
			this._Router = sap.ui.core.UIComponent.getRouterFor(this);
		},

		handleCreate: function (oEvent) {

			var lModel = this.getOwnerComponent().getModel("lModel");
			Utility._selectedRequestDetails = lModel.getProperty("/BP_Details");

			var aPartners = lModel.getProperty("/Partners");
			var i = 0;
			for (; aPartners[i];) {
				if (aPartners[i].PartnerType === Utility.keyContact) {
					if (Utility._partnerSeqNum < aPartners[i].PartnerSeqNum) {
						Utility._partnerSeqNum = aPartners[i].PartnerSeqNum;
					}
				}
				i++;
			}
			Utility._partnerType = Utility.keyContact;
			Utility._partnerSeqNum = Utility._partnerSeqNum + 1;
			Utility._partnerAction = Utility.reqTypeNew;

			lModel.setProperty("/BP_Details", {});

			var comModel = this.getOwnerComponent().getModel("comModel");
			comModel.setProperty("/CP/Email", []);
			comModel.setProperty("/CP/MobilePhone", []);
			comModel.setProperty("/CP/TelePhone", []);

			// visibility
			Utility.setCommunicationSections();

			this.getOwnerComponent().getModel("uiModel").setProperty("/ShowNameCP", true);
			this.getOwnerComponent().getModel("uiModel").setProperty("/ShowNameBP", false);

			this._Router.navTo("vContactDetail");
		},

		// 
		navToCPDetail: function (oEvent) {
			var oItem = oEvent.getParameter("listItem");
			var bindingContext = oItem.getBindingContext("lModel");
			var vPath = bindingContext.getPath();
			var oModel = bindingContext.getModel();
			var pDetails = oModel.getProperty(vPath);

			Utility._partnerType = Utility.keyContact;
			Utility._partnerSeqNum = pDetails.PartnerSeqNum;
			Utility._partnerAction = Utility.reqTypeUpdate;
			Utility._partnerPath = vPath;

			oModel.setProperty("/BP_Details", pDetails);

			var aComm = oModel.getProperty("/Communication");
			var bCommEmail = [];
			var bCommMob = [];
			var bCommTel = [];

			for (var i = 0; aComm[i]; i++) {
				if (aComm[i].PartnerType === Utility.keyContact && aComm[i].PartnerSeqNum === pDetails.PartnerSeqNum) {
					switch (aComm[i].CommType) {
					case Utility.keyEmail:
						bCommEmail.push(aComm[i]);
						break;
					case Utility.keyMob:
						bCommMob.push(aComm[i]);
						break;

					case Utility.keyTel:
						bCommTel.push(aComm[i]);
						break;
					}
				}
			}

			var comModel = this.getOwnerComponent().getModel("comModel");
			comModel.setProperty("/CP/Email", bCommEmail);
			comModel.setProperty("/CP/MobilePhone", bCommMob);
			comModel.setProperty("/CP/TelePhone", bCommTel);

			// visibility
			Utility.setCommunicationSections();

			this.getOwnerComponent().getModel("uiModel").setProperty("/ShowNameCP", true);
			this.getOwnerComponent().getModel("uiModel").setProperty("/ShowNameBP", false);

			this._Router.navTo("vContactDetail");
		},

		/**
		 * Similar to onAfterRendering, but this hook is invoked before the controller's View is re-rendered
		 * (NOT before the first rendering! onInit() is used for that one!).
		 * @memberOf ZCA.ZCA_BP_MAINTAIN.view.vContact
		 */
		onBeforeRendering: function () {
			Utility._vContact = this;
		}

		/**
		 * Called when the View has been rendered (so its HTML is part of the document). Post-rendering manipulations of the HTML could be done here.
		 * This hook is the same one that SAPUI5 controls get after being rendered.
		 * @memberOf ZCA.ZCA_BP_MAINTAIN.view.vContact
		 */
		//	onAfterRendering: function() {
		//
		//	},

		/**
		 * Called when the Controller is destroyed. Use this one to free resources and finalize activities.
		 * @memberOf ZCA.ZCA_BP_MAINTAIN.view.vContact
		 */
		//	onExit: function() {
		//
		//	}

	});

});