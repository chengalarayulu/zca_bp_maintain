sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"sap/m/MessageToast",
	"ZCA/ZCA_BP_MAINTAIN/controller/Utility"
], function (Controller, MToast, Utility) {
	"use strict";

	return Controller.extend("ZCA.ZCA_BP_MAINTAIN.controller.vContactDetail", {

		/**
		 * Called when a controller is instantiated and its View controls (if available) are already created.
		 * Can be used to modify the View before it is displayed, to bind event handlers and do other one-time initialization.
		 * @memberOf ZCA.ZCA_BP_MAINTAIN.view.vContactDetail
		 */
		onInit: function () {

		},

		onBack: function () {
			var lModel = this.getOwnerComponent().getModel("lModel");

			lModel.setProperty("/BP_Details", Utility._selectedRequestDetails);

			this.getOwnerComponent().getModel("uiModel").setProperty("/ShowNameCP", false);
			this.getOwnerComponent().getModel("uiModel").setProperty("/ShowNameBP", true);

			// reset to main BP
			Utility._partnerType = Utility.keyNoPartner;
			Utility._partnerSeqNum = 0;

			Utility.setCommunicationSections();

			var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
			oRouter.navTo("vDetail");
		},

		onSave: function () {

			var lModel = this.getOwnerComponent().getModel("lModel");
			var cDetails = lModel.getProperty("/BP_Details");
			var comModel = this.getOwnerComponent().getModel("comModel");
			//
			var comData = comModel.getProperty("/CP");

			switch (Utility._partnerAction) {
			case Utility.reqTypeNew:
				{
					// 
					cDetails.RequestNumber = Utility._newRequest;
					cDetails.PartnerType = Utility._partnerType;
					cDetails.PartnerSeqNum = Utility._partnerSeqNum;

					cDetails.HouseNumber = Utility._selectedRequestDetails.HouseNumber;
					cDetails.RoomNumber = Utility._selectedRequestDetails.RoomNumber;
					cDetails.BuildingNumber = Utility._selectedRequestDetails.BuildingNumber;
					cDetails.FloorNumber = Utility._selectedRequestDetails.FloorNumber;
					cDetails.StreetName = Utility._selectedRequestDetails.StreetName;
					cDetails.Street2 = Utility._selectedRequestDetails.Street2;
					cDetails.Street3 = Utility._selectedRequestDetails.Street3;
					cDetails.Street4 = Utility._selectedRequestDetails.Street4;
					cDetails.City = Utility._selectedRequestDetails.City;
					cDetails.District = Utility._selectedRequestDetails.District;
					cDetails.RegionState = Utility._selectedRequestDetails.RegionState;
					cDetails.PostalCode = Utility._selectedRequestDetails.PostalCode;
					cDetails.POBoxNumber = Utility._selectedRequestDetails.POBoxNumber;
					cDetails.CountryKey = Utility._selectedRequestDetails.CountryKey;
					cDetails.District = Utility._selectedRequestDetails.District;
					cDetails.PreferredCommType = Utility.getPreferredCommMethodPADisplay();

					// Emails
					var i = 0;
					for (; comData.Email[i];) {
						lModel.getProperty("/Communication").push(comData.Email[i]);
						i++;
					}
					// MobilePhone
					i = 0;
					for (; comData.MobilePhone[i];) {
						lModel.getProperty("/Communication").push(comData.MobilePhone[i]);
						i++;
					}
					// TelePhone
					i = 0;
					for (; comData.TelePhone[i];) {
						lModel.getProperty("/Communication").push(comData.TelePhone[i]);
						i++;
					}
					//
					lModel.getProperty("/Partners").push(cDetails);
					break;
				}
			case Utility.reqTypeUpdate:
				{
					lModel.setProperty(Utility._partnerPath, cDetails);
					Utility.setPreferredCommMethodPADisplay(cDetails.PreferredCommType);
					break;
				}
			}

			// reset to main BP
			Utility._partnerType = Utility.keyNoPartner;
			Utility._partnerSeqNum = 0;

			lModel.setProperty("/BP_Details", Utility._selectedRequestDetails);
			Utility.setCommunicationSections();

			MToast.show("Contact Saved");

			this.getOwnerComponent().getModel("uiModel").setProperty("/ShowNameCP", false);
			this.getOwnerComponent().getModel("uiModel").setProperty("/ShowNameBP", true);

			var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
			oRouter.navTo("vDetail");
		},

		/**
		 * Similar to onAfterRendering, but this hook is invoked before the controller's View is re-rendered
		 * (NOT before the first rendering! onInit() is used for that one!).
		 * @memberOf ZCA.ZCA_BP_MAINTAIN.view.vContactDetail
		 */
		onBeforeRendering: function () {
			Utility._vContactDetail = this;
		}

		/**
		 * Called when the View has been rendered (so its HTML is part of the document). Post-rendering manipulations of the HTML could be done here.
		 * This hook is the same one that SAPUI5 controls get after being rendered.
		 * @memberOf ZCA.ZCA_BP_MAINTAIN.view.vContactDetail
		 */
		//	onAfterRendering: function() {
		//
		//	},

		/**
		 * Called when the Controller is destroyed. Use this one to free resources and finalize activities.
		 * @memberOf ZCA.ZCA_BP_MAINTAIN.view.vContactDetail
		 */
		//	onExit: function() {
		//
		//	}

	});

});