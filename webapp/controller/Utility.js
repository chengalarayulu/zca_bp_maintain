sap.ui.define([
	"sap/m/MessageToast"
], function (MToast) {
	"use strict";

	return {
		// Global Variables
		Requester: "REQ",
		Validator: "VAL",
		CreditControl: "CRC",

		keySemObjRequester: "ZCA_BP_MAINTAIN_REQ",
		keySemObjValidator: "ZCA_BP_MAINTAIN_VAL",
		keySemObjCreditControl: "ZCA_BP_MAINTAIN_CRC",

		msgMandatory: "* Enter a value",
		msgChooseValue: "* Choose a value",

		Create: "CREATE",
		Display: "DISPLAY",
		Edit: "EDIT",

		actionSave: "SAVE",
		actionDelete: "DELETE",
		actionSubmit: "SUBMIT",
		actionApprove: "APPROVE",
		actionReject: "REJECT",

		keyNoPartner: "",
		keyBTP: "BP",
		keyPayer: "PY",
		keyContact: "CP",

		keyReqStatusDraft: "D",
		keyReqStatusValidating: "V",
		keyReqStatusCreditChecking: "C",
		keyReqStatusApproved: "A",
		keyReqStatusRejected: "R",

		highlightStatusD: "None",
		highlightStatusV: "Warning",
		highlightStatusC: "Success",
		highlightStatusA: "Success",
		highlightStatusR: "Error",

		keyAdSales: "SA",
		keyChannelSales: "SG",
		keyContentSales: "SC",
		keyAgencyCustomer: "AG",
		keyProformaCustomer: "PC",

		keyMob: "MOB",
		keyTel: "TEL",
		keyEmail: "EMAIL",
		keyFax: "FAX",

		keyDefaultEmail: "INT",
		keyDefaultPrint: "PRT",

		modeDelete: "Delete",
		modeNone: "None",

		reqTypeNew: "N",
		reqTypeUpdate: "U",
		reqTypeDelete: "D",

		_partnerType: "",
		_partnerSeqNum: 0,
		_viewsLoaded: false,
		_totalRequests: 0, // updated from vHome
		_newRequest: 0,

		vhSizeLimit: 999,

		reqType: "",

		_BTPExist: false,
		_ContactExist: false,
		_userRole: "REQ",
		_selectedRequestDetails: {},
		_selectedRequest: {},

		// _userRole					// _applMode
		// _selectedRequest				// _selectedPath
		//_lModelEmpty 

		_vGeneralName: null,
		_vGeneralComm: null,
		_vGeneralId: null,
		_vContact: null,
		_vAddData: null,
		_vComments: null,
		_vCreditData: null,
		_vHistory: null,
		_vSalesArea: null,
		_vDetail: null,
		_vGeneralAddress: null,
		_vContactDetail: null,
		_vBillToParty: null,
		_vCollections: null,
		_vFinance: null,
		_vPartnerDetail: null,
		_vTaxData: null,
		_vHome: null,
		that: this,
		changesMadeAfterSave: false,

		// Set userapi Model - get SSO user details
		setUserApiModel: function () {
			this.userModel = new sap.ui.model.json.JSONModel("/services/userapi/currentUser");
		},

		// 
		setBusy: function () {
			this._busy = new sap.m.BusyDialog();
			this._busy.open();
		},

		// 
		closeBusy: function () {
			this._busy.close();
		},

		// Get resource bundle for view - Not used
		resourceBundle: function (oView) {
			if (!this.resourceBundle) {
				this.resourceBundle = oView.getModel("i18n").getResourceBundle();
			}
			return this.resourceBundle;
		},

		// Get user role from URL parameters
		setUserRole: function () {
			this._userRole = jQuery.sap.getUriParameters().get("ROLE");

			// this._userRole = this.Requester; // To be removed - for SAP investigation

			if (this._userRole === null || this._userRole === "") {
				var url = window.location.search;
				// var url = urlEntity.search;

				if (url.indexOf("req=") !== -1) {
					this._userRole = this.Requester;
				}

				if ((url.indexOf("val=") !== -1)) {
					this._userRole = this.Validator;
				}

				if ((url.indexOf("crc=") !== -1)) {
					this._userRole = this.CreditControl;
				}

				// var urlHash = window.location.hash;
				// var oParser = new sap.ushell.services.URLParsing();
				// var urlParams = oParser.parseShellHash(urlHash);
				// switch (urlParams.semanticObject) {
				// case this.keySemObjRequester:
				// 	this._userRole = this.Requester;
				// 	break;
				// case this.keySemObjValidator:
				// 	this._userRole = this.Validator;
				// 	break;
				// case this.keySemObjCreditControl:
				// 	this._userRole = this.CreditControl;
				// 	break;
				// default:
				// 	this._userRole = this.Requester;
				// }
			}
		},

		// Set button create visible 
		setCreateButton: function () {
			var uiModel = this._Component.getModel("uiModel");
			if (this._userRole === this.Requester) {
				uiModel.setProperty("/ShowBtnCreate", true);
			} else {
				uiModel.setProperty("/ShowBtnCreate", false);
			}
		},

		// set Title 
		setPageTitle: function () {
			var uiModel = this._Component.getModel("uiModel");

			var vTitle = this._Component.getModel("i18n").getResourceBundle().getText("appTitleReq");
			switch (this._userRole) {
			case this.Validator:
				vTitle = this._Component.getModel("i18n").getResourceBundle().getText("appTitleVal");
				break;
			case this.CreditControl:
				vTitle = this._Component.getModel("i18n").getResourceBundle().getText("appTitleCrc");
				break;
			}
			uiModel.setProperty("/PageTitle", vTitle);
		},

		// Set local model for Create mode
		setLocalModelCreate: function () {
			this._lModel = this._Component.getModel("lModel");
			this._lModel.setProperty("/Header", {});
			this._lModel.setProperty("/BP_Details", {});
			this._lModel.setProperty("/Partners", []);
			this._lModel.setProperty("/Communication", []);
			this._lModel.setProperty("/TaxData", []);
			this._lModel.setProperty("/SalesArea", []);
			this._lModel.setProperty("/History", []);

			var that = this;
			this._Component.getModel("vhColl").read("/ZCA_C_COLDEFAULTGRP", {
				success: function (oData, oResponse) {
					var cArray = [],
						i = 0;
					for (; oData.results[i];) {
						var cData = {
							CollSegment: oData.results[i].CollSegment,
							CollGroup: oData.results[i].CollGroup
						};
						cArray.push(cData);
						i++;
					}
					that._lModel.setProperty("/Collections", cArray);
				},
				error: function (oError) {
					that._xsError = "Collections";
				}
			});
		},

		// set View specific models  - Not used
		setLocalModel: function () {
			// Cloud model

			this._lModel = this._Component.getModel("lModel");
			this._lModelEmpty = this._lModel;
			this._lModel.setProperty("/Header", this._selectedRequest);

			var that = this;
			var i = 0;
			var array = [];

			var xsModel = this._Component.getModel("xsModel");
			// After /toDetails has been loaded set UI configs 
			xsModel.attachEventOnce("requestCompleted", function (oEvent) {
				that.setUIConfigs(that._vHome);
			}, this);
			//

			// A_Details
			var ContextLocation = this._selectedPath + "/toDetails";
			xsModel.read(ContextLocation, {
				success: function (oData, oResponse) {
					i = 0;
					array = [];
					that._BTPExist = false;
					that._ContactExist = false;
					for (; oData.results[i];) {
						if (oData.results[i].PartnerType === that.keyNoPartner) {
							oData.results[i].Comments = null;
							that._lModel.setProperty("/BP_Details", oData.results[i]);
							that._selectedRequestDetails = oData.results[i];
						} else {
							switch (oData.results[i].PartnerType) {
							case that.keyBTP:
								that._BTPExist = true;
								break;
							case that.keyContact:
								that._ContactExist = true;
								break;
							}
							array.push(oData.results[i]);
						}
						i++;
					}
					that._lModel.setProperty("/Partners", array);
				},
				error: function (oEvent) {
					that._xsError = "Details";
				}
			}); // End of A_Details

			// A_Communication
			ContextLocation = this._selectedPath + "/toComm";
			xsModel.read(ContextLocation, {
				success: function (oData, oResponse) {
					i = 0;
					array = [];
					for (; oData.results[i];) {
						oData.results[i].DefaultComm = (oData.results[i].DefaultComm === "true") ? true : false;
						array.push(oData.results[i]);
						i++;
					}
					that._lModel.setProperty("/Communication", array);
				},
				error: function (oEvent) {
					that._xsError = "Comm";
				}
			}); // End of A_Communication

			// A_Identification
			ContextLocation = this._selectedPath + "/toIdentification";
			xsModel.read(ContextLocation, {
				success: function (oData, oResponse) {
					i = 0;
					array = [];
					for (; oData.results[i];) {
						array.push(oData.results[i]);
						i++;
					}
					that._lModel.setProperty("/Identification", array);
				},
				error: function (oEvent) {
					that._xsError = "Details";
				}
			}); // End of A_Identification

			// A_TaxData
			ContextLocation = this._selectedPath + "/toTaxData";
			xsModel.read(ContextLocation, {
				success: function (oData, oResponse) {
					i = 0;
					array = [];
					for (; oData.results[i];) {
						array.push(oData.results[i]);
						i++;
					}
					that._lModel.setProperty("/TaxData", array);
				},
				error: function (oEvent) {
					that._xsError = "TaxData";
				}
			}); // End of A_TaxData

			// A_SalesArea
			ContextLocation = this._selectedPath + "/toSalesArea";
			xsModel.read(ContextLocation, {
				success: function (oData, oResponse) {
					i = 0;
					array = [];
					for (; oData.results[i];) {
						array.push(oData.results[i]);
						i++;
					}
					that._lModel.setProperty("/SalesArea", array);
				},
				error: function (oEvent) {
					that._xsError = "TaxData";
				}
			}); // End of A_SalesArea

			// A_Collections
			ContextLocation = this._selectedPath + "/toCollections";
			xsModel.read(ContextLocation, {
				success: function (oData, oResponse) {
					i = 0;
					array = [];
					for (; oData.results[i];) {
						array.push(oData.results[i]);
						i++;
					}
					that._lModel.setProperty("/Collections", array);
				},
				error: function (oEvent) {
					that._xsError = "Collections";
				}
			}); // End of A_Collections			

			// A_History
			ContextLocation = this._selectedPath + "/toHistory";
			xsModel.read(ContextLocation, {
				success: function (oData, oResponse) {
					i = 0;
					array = [];
					for (; oData.results[i];) {
						array.push(oData.results[i]);
						i++;
					}
					that._lModel.setProperty("/History", array);
				},
				error: function (oEvent) {
					that._xsError = "History";
				}
			}); // End of A_History	
		},
		//
		getNewRequest: function () {
			// New Request number 
			var ContextLocation = "/A_NextNumber";

			var xsModel = this._Component.getModel("xsModel");
			var that = this;
			xsModel.read(ContextLocation, {
				success: function (oData, oResponse) {
					var i = 0;
					for (; oData.results[i];) {
						that._newRequest = oData.results[i].NextNumber;
						i++;
					}
					if (that._newRequest === 0 || that._newRequest === null) {
						that._newRequest = 1;
					}
					// Create
					that._applMode = that.Create;
					//
					that.setLocalModelCreate();
					that._selectedRequest = null;
					that.reqType = that.reqTypeNew;
					//
					that.setUIConfigs(that._vHome);
					// 
					var oRouter = sap.ui.core.UIComponent.getRouterFor(that._vHome);
					oRouter.navTo("vDetail");
				},
				error: function (oEvent) {
					MToast.show("Could not generate request number, refresh the page");
					that._xsError = "NextNumber";
				}
			}); // End of 		
		},

		// Get address field configs 
		getAddressFieldConfigs: function () {
			var ContextLocation = "/ZZ1_CA_BP_ADRFIELDS_CONFIG";
			var adrModel = this._Component.getModel("adrFields");

			var that = this;
			adrModel.read(ContextLocation, {
				success: function (oData, oResponse) {
					that._Component.setModel(oData.results, "adrModel");
				},
				error: function (oEvent) {

				}
			});

		},

		// Get UUID for history 
		getUUID: function () {
			var dt = new Date().getTime();
			var uuid = "xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxx".replace(/[xy]/g, function (c) {
				var r = (dt + Math.random() * 16) % 16 | 0;
				dt = Math.floor(dt / 16);
				return (c === "x" ? r : (r & 0x3 | 0x8)).toString(16);
			});
			return uuid;
		},

		//
		getStatusColorScheme: function () {
			switch (this._selectedRequest.ReqStatus) {
			case this.keyReqStatusValidating:
				return 2; // Orange
			case this.keyReqStatusCreditChecking:
				return 5; // Blue
			case this.keyReqStatusApproved:
				return 8; // Green
			case this.keyReqStatusRejected:
				return 3; // Red
			default:
				return 9; // Grey
			}
		},

		// Get Status Text 
		getStatusText: function (sStatus) {
			if (!sStatus) {
				var lStatus = this.keyReqStatusDraft;
			} else {
				lStatus = sStatus;
			}
			var oResourceBundle = this.getView().getModel("i18n").getResourceBundle();
			var bundleText = "reqStatus" + lStatus;
			return oResourceBundle.getText(bundleText);
		},

		// Get list Highlight value
		getItemHighlight: function (sStatus, sRole) {
			if (!sStatus) {
				var lStatus = this.keyReqStatusDraft;
			} else {
				if (sRole === "CRC") {
					lStatus = "V"; // only to get highlight 
				} else {
					lStatus = sStatus;
				}

			}
			var oResourceBundle = this.getView().getModel("i18n").getResourceBundle();
			var bundleText = "highlightStatus" + lStatus;
			return oResourceBundle.getText(bundleText);
		},

		// Format Request Number
		formatRequestNumber: function (vReqNumber) {
			return "REQ-" + vReqNumber;
		},

		// Get List title - home page
		getListTitle: function (vName, vNumber) {
			if (vNumber) {
				return (vName + "/" + vNumber);
			} else {
				return vName;
			}
		},

		// Get List title - home page
		getHistoryInfo: function (pRole, pUser) {
			switch (pRole) {
			case "REQ":
				return "Team - Requesters, User - " + pUser;
			case "VAL":
				return "Team - Validators, User - " + pUser;
			case "CRC":
				return "Team - Credit Control, User - " + pUser;
			default:
				return "Team - Requesters, User - " + pUser;
			}
		},

		// 
		getPreferredCommMethodPADisplay: function () {
			var uiModel = this._Component.getModel("uiModel");
			var mEntity = uiModel.getProperty("/");

			// Email
			if (mEntity.StateEmailPA === true) {
				var commMethod = this.keyDefaultEmail;
			}
			// MobilePhone
			if (mEntity.StateMobPA === true) {
				commMethod = this.keyMob;
			}
			// Telephone
			if (mEntity.StateTelPA === true) {
				commMethod = this.keyTel;
			}
			// MobilePhone
			if (mEntity.StatePrintPA === true) {
				commMethod = this.keyPrint;
			}
			return commMethod;
		},

		// 
		setPreferredCommMethodPADisplay: function (sCommMethod) {

			var uiModel = this._Component.getModel("uiModel");
			var mEntity = uiModel.getProperty("/");

			// Email
			switch (sCommMethod) {
			case this.keyDefaultEmail:
				mEntity.StateTelPA = false;
				mEntity.StateEmailPA = true;
				mEntity.StateMobPA = false;
				mEntity.StatePrintPA = false;
				break;
			case this.keyMob:
				mEntity.StateTelPA = false;
				mEntity.StateEmailPA = false;
				mEntity.StateMobPA = true;
				mEntity.StatePrintPA = false;
				break;
			case this.keyTel:
				mEntity.StateTelPA = true;
				mEntity.StateEmailPA = false;
				mEntity.StateMobPA = false;
				mEntity.StatePrintPA = false;
				break;
			case this.keyPrint:
				mEntity.StateTelPA = false;
				mEntity.StateEmailPA = false;
				mEntity.StateMobPA = false;
				mEntity.StatePrintPA = true;
				break;
			}
		},

		// Set Communication section visible 
		setCommunicationSections: function () {

			var uiModel = this._Component.getModel("uiModel");

			switch (this._partnerType) {
			case this.keyBTP:
				var showCommCust = false;
				var showCommBTP = true;
				var showCommCP = false;
				break;
			case this.keyContact:
				showCommCust = false;
				showCommBTP = false;
				showCommCP = true;
				break;
			default:
				showCommCust = true;
				showCommBTP = false;
				showCommCP = false;
			}

			uiModel.setProperty("/showCommCust", showCommCust);
			uiModel.setProperty("/showCommBTP", showCommBTP);
			uiModel.setProperty("/showCommCP", showCommCP);
		},

		// Set UI configurations
		setUIConfigs: function (oObject) {

			// set communication sections 
			this.setCommunicationSections();

			var uiModel = oObject.getOwnerComponent().getModel("uiModel");
			var mEntity = uiModel.getProperty("/");

			mEntity.StateBTP = this._BTPExist;
			mEntity.StateContact = this._ContactExist;

			// Additional Attributes 
			if (this._selectedRequestDetails.AdSales === this.keyAdSales || mEntity.AdSales === true) {
				mEntity.AdSales = true;
			} else {
				mEntity.AdSales = false;
			}

			// Channel Sales
			if (this._selectedRequestDetails.ChannelSales === this.keyChannelSales || mEntity.ChannelSales === true) {
				mEntity.ChannelSales = true;
			} else {
				mEntity.ChannelSales = false;
			}

			// Content Sales
			if (this._selectedRequestDetails.ContentSales === this.keyContentSales || mEntity.ContentSales === true) {
				mEntity.ContentSales = true;
			} else {
				mEntity.ContentSales = false;
			}

			// Agency Customer
			if (this._selectedRequestDetails.AgencyCustomer === this.keyAgencyCustomer || mEntity.AgencyCustomer === true) {
				mEntity.AgencyCustomer = true;
			} else {
				mEntity.AgencyCustomer = false;
			}

			// Proforma Customer
			if (this._selectedRequestDetails.ProformaCustomer === this.keyProformaCustomer || mEntity.ProformaCustomer === true) {
				mEntity.ProformaCustomer = true;
			} else {
				mEntity.ProformaCustomer = false;
			}

			// Not Relevant
			if (!mEntity.AdSales && !mEntity.ChannelSales && !mEntity.ContentSales && !mEntity.ProformaCustomer && !mEntity.AgencyCustomer) {
				mEntity.NotRelevant = true;
			} else {
				mEntity.NotRelevant = false;
			}

			// Dunning Procedure 
			mEntity.stateDunning = (this._selectedRequestDetails.DunningProcedure !== null ||
				this._selectedRequestDetails.DunningProcedure !== "") && this._selectedRequestDetails.DunningProcedure ? true : false;

			// objectSubTitle 
			if (this._selectedRequest) {
				mEntity.objectSubTitle = "REQ-" + this._selectedRequest.RequestNumber;
				if (this._selectedRequest.BPNumber) {
					mEntity.objectSubTitle = mEntity.objectSubTitle + " / " + this._selectedRequest.BPNumber;
				}
			} else {
				mEntity.objectSubTitle = "REQ-" + this._newRequest;
			}

			// Requester
			switch (this._userRole) {
			case this.Requester:
				// mEntity.PageTitle = oObject.getView().getModel("i18n").getResourceBundle().getText("appTitleReq");

				mEntity.EnableComments = true;
				mEntity.ShowBtnEdit = true;
				switch (this._applMode) {
				case this.Create: // Requester
					mEntity.ShowBtnEdit = false;
					mEntity.ShowBtnDelete = false;
					mEntity.ShowBtnSave = true;
					mEntity.ShowBtnSubmit = true;
					mEntity.IconEdit = "sap-icon://display";

					// Set Application Mode(DISPLAY/EDIT) based UI configurations					
					mEntity.EnableGeneral = true;
					mEntity.EnableSales = true;
					mEntity.EnableFinance = true;
					mEntity.ShowCredit = false;
					mEntity.ShowCollections = false;

					// Set Application Mode(DISPLAY/EDIT) based UI configurations for Partner sections					
					mEntity.EnableBTP = true;
					mEntity.EnablePayer = true;
					mEntity.EnableContact = true;
					mEntity.StateBTP = false;
					mEntity.StatePayer = false;
					mEntity.StateContact = false;

					mEntity.ShowCollections = false;
					mEntity.ShowCredit = false;

					mEntity.RequiredDistrict = false;
					mEntity.RequiredRegion = false;
					mEntity.RequiredPostalcode = false;
					mEntity.RequiredPOBox = false;

					break;
				case this.Edit: // Requester
					mEntity.ShowBtnDelete = true;
					mEntity.ShowBtnSave = true;
					mEntity.ShowBtnSubmit = true;
					mEntity.IconEdit = "sap-icon://display";

					// Set Application Mode(DISPLAY/EDIT) based UI configurations					
					mEntity.EnableGeneral = true;
					mEntity.EnableSales = true;
					mEntity.EnableFinance = true;

					mEntity.EnableContact = true;
					mEntity.EnableBTP = true;
					mEntity.EnablePayer = true;
					mEntity.StatePayer = true;

					mEntity.ShowCollections = false;
					mEntity.ShowCredit = false;

					break;
				case this.Display: // Requester

					if ((this._selectedRequest.ReqStatus !== this.keyReqStatusDraft) &&
						(this._selectedRequest.ReqStatus !== this.keyReqStatusRejected)) {
						mEntity.ShowBtnEdit = false;
					}

					mEntity.EnableComments = false;
					mEntity.ShowBtnDelete = false;
					mEntity.ShowBtnSave = false;
					mEntity.ShowBtnSubmit = false;
					mEntity.IconEdit = "sap-icon://edit";

					// Set Application Mode(DISPLAY/EDIT) based UI configurations					
					mEntity.EnableGeneral = false;
					mEntity.EnableSales = false;
					mEntity.EnableFinance = false;

					mEntity.EnableBTP = false;
					mEntity.EnablePayer = false;
					mEntity.EnableContact = false;
					mEntity.StatePayer = false;

					mEntity.ShowCollections = true;
					mEntity.ShowCredit = true;

					break;
				}

				mEntity.ShowBtnCreate = true;
				break;
				// Validator
			case this.Validator:

				// mEntity.PageTitle = oObject.getView().getModel("i18n").getResourceBundle().getText("appTitleVal");
				mEntity.ShowBtnApprove = true;
				mEntity.ShowBtnReject = true;
				mEntity.ShowBtnEdit = true;
				mEntity.ShowBtnCreate = false;

				switch (this._applMode) {
				case this.Edit: // Validator
					mEntity.IconEdit = "sap-icon://display";

					mEntity.EnableComments = true;
					// Set Application Mode(DISPLAY/EDIT) based UI configurations					
					mEntity.EnableGeneral = true;
					mEntity.EnableSales = true;
					mEntity.EnableFinance = true;

					// Set Application Mode(DISPLAY/EDIT) based UI configurations for Partner sections					
					mEntity.EnableBTP = true;
					mEntity.EnablePayer = true;
					mEntity.EnableContact = true;

					mEntity.ShowCollections = false;
					mEntity.ShowCredit = false;

					break;

				case this.Display: // Validator

					if ((this._selectedRequest.ReqStatus !== this.keyReqStatusValidating) &&
						(this._selectedRequest.ReqStatus !== this.keyReqStatusRejected)) {
						mEntity.ShowBtnEdit = false;
						mEntity.ShowBtnApprove = false;
						mEntity.ShowBtnReject = false;
					}

					mEntity.EnableComments = false;
					mEntity.IconEdit = "sap-icon://edit";

					// Set Application Mode(DISPLAY/EDIT) based UI configurations					
					mEntity.EnableGeneral = false;
					mEntity.EnableSales = false;
					mEntity.EnableFinance = false;

					// Set Application Mode(DISPLAY/EDIT) based UI configurations for Partner sections					
					mEntity.EnableBTP = false;
					mEntity.EnablePayer = false;
					mEntity.EnableContact = false;

					mEntity.ShowCollections = true;
					mEntity.ShowCredit = true;
					break;
				}
				break;
				// Credit Controller
			case this.CreditControl:

				mEntity.ShowBtnCreate = false;

				if (this._applMode === this.Display) {
					if (this._selectedRequest.ReqStatus !== this.keyReqStatusCreditChecking) {
						mEntity.ShowBtnEdit = false;
						mEntity.ShowBtnApprove = false;
						mEntity.ShowBtnReject = false;
					} else {
						mEntity.ShowBtnApprove = true;
						mEntity.ShowBtnReject = true;
					}
				}

				// mEntity.PageTitle = oObject.getView().getModel("i18n").getResourceBundle().getText("appTitleCrc");

				mEntity.EnableComments = true;
				mEntity.EnableCredit = true;
				mEntity.EnableCollections = true;

				break;
			default:
				// mEntity.PageTitle = oObject.getView().getModel("i18n").getResourceBundle().getText("appTitleReq");
			}
			// 
			var bundleText;
			if (this._selectedRequest) {
				mEntity.StatusColorScheme = this.getStatusColorScheme();
				bundleText = "reqStatus" + this._selectedRequest.ReqStatus;
			} else {
				mEntity.StatusColorScheme = 9;
				bundleText = "reqStatus" + this.keyReqStatusDraft;
			}
			mEntity.StatusText = oObject.getView().getModel("i18n").getResourceBundle().getText(bundleText);
			//
			if (this._partnerType === this.keyContact) {
				mEntity.ShowNameCP = true;
				mEntity.ShowNameBP = false;
			} else {
				mEntity.ShowNameCP = false;
				mEntity.ShowNameBP = true;
			}

			//
			uiModel.setProperty("/", mEntity);
		},

		// ************************************************************************* 
		setViewUIbindings: function (partnerType) {

			if (!this._viewsLoaded) {
				return;
			}
			this._setBindingsGeneralComm(this._vGeneralComm, partnerType);
			// this._setBindingsGeneralId(this._vGeneralId);
			// if (partnerType === this.keyContact){
			this._setBindingsContacts(this._vContact);
			// }

			// if(partnerType === this.keyBTP){
			this._setBindingsBTP(this._vBillToParty);
			// }
		},

		// Communication 
		_setBindingsGeneralComm: function (oObject, partnerType) {
			// Main Customer

			this._setSectionComm(oObject, "idlEmail", this.keyEmail);
			// this._setSectionComm(oObject, "idlMobilePhone", this.keyMob);
			this._setSectionComm(oObject, "idlTelePhone", this.keyTel);
			// this._setSectionComm(oObject, "idlFax", this.keyFax);

			// // BTP 
			// if (partnerType === this.keyBTP) {
			// 	this._setSectionComm(oObject, "idlEmailBP", this.keyEmail);
			// 	this._setSectionComm(oObject, "idlMobilePhoneBP", this.keyMob);
			// 	this._setSectionComm(oObject, "idlTelePhoneBP", this.keyTel);
			// }

			// // CP 
			// if (partnerType === this.keyContact) {
			// 	this._setSectionComm(oObject, "idlEmailCP", this.keyEmail);
			// 	this._setSectionComm(oObject, "idlMobilePhoneCP", this.keyMob);
			// 	this._setSectionComm(oObject, "idlTelePhoneCP", this.keyTel);
			// }
		},

		_setSectionComm: function (oObject, sId, sFilterKey) {
			// build filter array
			var aFilter = [];

			aFilter.push(new sap.ui.model.Filter("PartnerType", sap.ui.model.FilterOperator.EQ, this._partnerType));
			aFilter.push(new sap.ui.model.Filter("PartnerSeqNum", sap.ui.model.FilterOperator.EQ, this._partnerSeqNum));
			aFilter.push(new sap.ui.model.Filter("CommType", sap.ui.model.FilterOperator.EQ, sFilterKey));

			// Bind model path to view
			var bElement = oObject.getView().byId(sId);
			var oBindings = bElement.getBinding("items");
			oBindings.filter(aFilter, true);
			oBindings.getModel().updateBindings(true);
			oBindings.getModel().refresh(true);
		},

		// Identification 
		_setBindingsGeneralId: function (oObject) {
			//
			var aFilter = [];
			aFilter.push(new sap.ui.model.Filter("PartnerType", sap.ui.model.FilterOperator.EQ, this._partnerType));
			aFilter.push(new sap.ui.model.Filter("PartnerSeqNum", sap.ui.model.FilterOperator.EQ, this._partnerSeqNum));

			var bElement = oObject.getView().byId("idtIdentification");
			var oBindings = bElement.getBinding("items");
			oBindings.filter(aFilter);
		},

		// Contacts 
		_setBindingsContacts: function (oObject) {
			if (!oObject) {
				return;
			}
			//
			var aFilter = [];
			aFilter.push(new sap.ui.model.Filter("PartnerType", sap.ui.model.FilterOperator.EQ, this.keyContact));

			var bElement = oObject.getView().byId("idtContacts");
			var oBindings = bElement.getBinding("items");
			oBindings.filter(aFilter);
		},

		// Bill To Party 
		_setBindingsBTP: function (oObject) {
			if (!oObject) {
				return;
			}
			//
			var aFilter = [];
			aFilter.push(new sap.ui.model.Filter("PartnerType", sap.ui.model.FilterOperator.EQ, this.keyBTP));

			var bElement = oObject.getView().byId("idtBillToParty");
			var oBindings = bElement.getBinding("items");
			oBindings.filter(aFilter);
		},
		// 
		setPreferredCommMethod: function (isPartner, pKey) {
			var uiModel = this._Component.getModel("uiModel");
			var uiData = uiModel.getProperty("/");

			if (isPartner) {
				switch (pKey) {
				case this.keyEmail:
					{
						uiData.StateEmailPA = true;
						uiData.StateTelPA = false;
						uiData.StateMobPA = false;
						uiData.StatePrintPA = false;
						break;
					}
				case this.keyTel:
					{
						uiData.StateEmailPA = false;
						uiData.StateTelPA = true;
						uiData.StateMobPA = false;
						uiData.StatePrintPA = false;
						break;
					}
				case this.keyMob:
					{
						uiData.StateEmailPA = false;
						uiData.StateTelPA = false;
						uiData.StateMobPA = true;
						uiData.StatePrintPA = false;
						break;
					}
				case this.keyPrint:
					{
						uiData.StateEmailPA = false;
						uiData.StateTelPA = false;
						uiData.StateMobPA = false;
						uiData.StatePrintPA = true;
						break;
					}
				}
			}
			// Customer 
			else {
				switch (pKey) {
				case this.keyEmail:
					{
						uiData.StateEmail = true;
						uiData.StateTel = false;
						uiData.StateMob = false;
						uiData.StatePrint = false;
						break;
					}
				case this.keyTel:
					{
						uiData.StateEmail = false;
						uiData.StateTel = true;
						uiData.StateMob = false;
						uiData.StatePrint = false;
						break;
					}
				case this.keyMob:
					{
						uiData.StateEmail = false;
						uiData.StateTel = false;
						uiData.StateMob = true;
						uiData.StatePrint = false;
						break;
					}
				case this.keyPrint:
					{
						uiData.StateEmail = false;
						uiData.StateTel = false;
						uiData.StateMob = true;
						uiData.StatePrint = true;
						break;
					}
				}
			}
			//
			uiModel.setProperty("/", uiData);
		}

	};
});