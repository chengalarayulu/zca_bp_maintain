sap.ui.define(["sap/uxap/BlockBase"], function (BlockBase) {
	"use strict";
	var myBlock = BlockBase.extend("ZCA.ZCA_BP_MAINTAIN.controller.blocks.TaxData", {
		metadata: {
			views: {
				Collapsed: {
					viewName: "ZCA.ZCA_BP_MAINTAIN.view.vTaxData",
					type: "XML"
				},
				Expanded: {
					viewName: "ZCA.ZCA_BP_MAINTAIN.view.vTaxData",
					type: "XML"
				}
			}
		}
	});
	return myBlock;
}, true);