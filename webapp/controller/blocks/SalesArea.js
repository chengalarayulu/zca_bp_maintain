sap.ui.define(["sap/uxap/BlockBase"], function (BlockBase) {
	"use strict";
	var myBlock = BlockBase.extend("ZCA.ZCA_BP_MAINTAIN.controller.blocks.SalesArea", {
		metadata: {
			views: {
				Collapsed: {
					viewName: "ZCA.ZCA_BP_MAINTAIN.view.vSalesArea",
					type: "XML"
				},
				Expanded: {
					viewName: "ZCA.ZCA_BP_MAINTAIN.view.vSalesArea",
					type: "XML"
				}
			}
		}
	});
	return myBlock;
}, true);