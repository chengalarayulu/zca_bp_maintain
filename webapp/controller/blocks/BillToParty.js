sap.ui.define(["sap/uxap/BlockBase"], function (BlockBase) {
	"use strict";
	var myBlock = BlockBase.extend("ZCA.ZCA_BP_MAINTAIN.controller.blocks.BillToParty", {
		metadata: {
			views: {
				Collapsed: {
					viewName: "ZCA.ZCA_BP_MAINTAIN.view.vBillToParty",
					type: "XML"
				},
				Expanded: {
					viewName: "ZCA.ZCA_BP_MAINTAIN.view.vBillToParty",
					type: "XML"
				}
			}
		}
	});
	return myBlock;
}, true);