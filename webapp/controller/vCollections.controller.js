sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"ZCA/ZCA_BP_MAINTAIN/controller/Utility"
], function (Controller, Utility) {
	"use strict";

	return Controller.extend("ZCA.ZCA_BP_MAINTAIN.controller.vCollections", {

		/**
		 * Called when a controller is instantiated and its View controls (if available) are already created.
		 * Can be used to modify the View before it is displayed, to bind event handlers and do other one-time initialization.
		 * @memberOf ZCA.ZCA_BP_MAINTAIN.view.vCollections
		 */
		onInit: function () {

		},

		/**
		 * Similar to onAfterRendering, but this hook is invoked before the controller's View is re-rendered
		 * (NOT before the first rendering! onInit() is used for that one!).
		 * @memberOf ZCA.ZCA_BP_MAINTAIN.view.vCollections
		 */
		onBeforeRendering: function () {
			Utility._vCollections = this;
		}

		/**
		 * Called when the View has been rendered (so its HTML is part of the document). Post-rendering manipulations of the HTML could be done here.
		 * This hook is the same one that SAPUI5 controls get after being rendered.
		 * @memberOf ZCA.ZCA_BP_MAINTAIN.view.vCollections
		 */
		//	onAfterRendering: function() {
		//
		//	},

		/**
		 * Called when the Controller is destroyed. Use this one to free resources and finalize activities.
		 * @memberOf ZCA.ZCA_BP_MAINTAIN.view.vCollections
		 */
		//	onExit: function() {
		//
		//	}

	});

});