sap.ui.define(["sap/ui/core/mvc/Controller", "ZCA/ZCA_BP_MAINTAIN/controller/Utility"], function (Controller, Utility) {
	"use strict";
	return Controller.extend("ZCA.ZCA_BP_MAINTAIN.controller.vHome", {
		/**
		 * Called when a controller is instantiated and its View controls (if available) are already created.
		 * Can be used to modify the View before it is displayed, to bind event handlers and do other one-time initialization.
		 * @memberOf ZCA.ZCA_BP_MAINTAIN.view.vHome
		 */

		utility: Utility,

		onInit: function () {

			Utility.setPageTitle();
			this.setCreateButton();
			Utility.getAddressFieldConfigs();
		},
		// 
		setCreateButton: function () {
			var cButton = this.getView().byId("idCreateButton");
			if (Utility._userRole === Utility.Requester) {
				cButton.setVisible(true);
			} else {
				cButton.setVisible(false);
			}
		},
		// 
		navToDetail: function (oEvent) {

			var binContext = oEvent.getSource().getBindingContext("xsModel");

			Utility._selectedPath = binContext.getPath();
			Utility._selectedRequest = binContext.getModel().getProperty(Utility._selectedPath);
			// Display
			Utility._applMode = Utility.Display;
			//
			Utility.reqType = Utility.reqTypeUpdate;
			// 
			Utility.setLocalModel();
			//
			var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
			oRouter.navTo("vDetail");
		},

		onCreate: function () {

			// // Create
			// Utility._applMode = Utility.Create;
			// New number
			Utility.getNewRequest();
			// //
			// Utility.getAddressFieldConfigs();
			// Utility.setLocalModelCreate();
			// Utility._selectedRequest = null;

			// Utility.reqType = Utility.reqTypeNew;
			// //
			// Utility.setUIConfigs(this);

			// var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
			// oRouter.navTo("vDetail");
		},

		onChange: function (oEvent) {

			var oListBinding = this.getView().byId("idCustList").getBinding("items");
			var sValue = oEvent.getParameter("newValue");
			if (sValue) {

				var uValue = sValue.toUpperCase();

				// build filter array
				var aFilter = Object.assign([], this._roleFilter);

				aFilter.push(new sap.ui.model.Filter("CustName1", sap.ui.model.FilterOperator.Contains, sValue));
				// aFilter.push(new sap.ui.model.Filter("RequestNumber", sap.ui.model.FilterOperator.Contains, sValue));
				// aFilter.push(new sap.ui.model.Filter("BPNumber", sap.ui.model.FilterOperator.Contains, sValue));

				aFilter.push(new sap.ui.model.Filter("CustName1", sap.ui.model.FilterOperator.Contains, uValue));
				// aFilter.push(new sap.ui.model.Filter("RequestNumber", sap.ui.model.FilterOperator.Contains, uValue));
				// aFilter.push(new sap.ui.model.Filter("BPNumber", sap.ui.model.FilterOperator.Contains, uValue));

				oListBinding.filter(aFilter, false);

				// 	{
				// 	filters: [new sap.ui.model.Filter("CustName1", sap.ui.model.FilterOperator.Contains, sValue),
				// 		new sap.ui.model.Filter("CustName1", sap.ui.model.FilterOperator.Contains, uValue)
				// 	],
				// 	and: false
				// });

			} else {
				oListBinding.filter(this._roleFilter);
			}
		},

		/**
		 * Similar to onAfterRendering, but this hook is invoked before the controller's View is re-rendered
		 * (NOT before the first rendering! onInit() is used for that one!).
		 * @memberOf ZCA.ZCA_BP_MAINTAIN.view.vHome
		 */
		onBeforeRendering: function () {
				Utility._vHome = this;
				//
				var oListBinding = this.getView().byId("idCustList").getBinding("items");

				// build filter array
				this._roleFilter = [];
				switch (Utility._userRole) {
				case Utility.Validator:
					this._roleFilter.push(new sap.ui.model.Filter("ReqStatus", sap.ui.model.FilterOperator.NE, Utility.keyReqStatusDraft));
					// aFilter.push(new sap.ui.model.Filter("ReqStatus", sap.ui.model.FilterOperator.EQ, Utility.keyReqStatusCreditChecking          ));	
					// aFilter.push(new sap.ui.model.Filter("ReqStatus", sap.ui.model.FilterOperator.EQ, Utility.keyReqStatusRejected          ));
					break;

				case Utility.CreditControl:
					this._roleFilter.push(new sap.ui.model.Filter("ReqStatus", sap.ui.model.FilterOperator.EQ, Utility.keyReqStatusCreditChecking));
					// aFilter.push(new sap.ui.model.Filter("ReqStatus", sap.ui.model.FilterOperator.NE, Utility.keyReqStatusValidating));
					break;
				}

				oListBinding.filter(this._roleFilter);
			}
			/**
			 * Called when the View has been rendered (so its HTML is part of the document). Post-rendering manipulations of the HTML could be done here.
			 * This hook is the same one that SAPUI5 controls get after being rendered.
			 * @memberOf ZCA.ZCA_BP_MAINTAIN.view.vHome
			 */
			// onAfterRendering: function () {

		// }
		/**
		 * Called when the Controller is destroyed. Use this one to free resources and finalize activities.
		 * @memberOf ZCA.ZCA_BP_MAINTAIN.view.vHome
		 */
		//	onExit: function() {
		//
		//	}

		/**
		 *@memberOf ZCA.ZCA_BP_MAINTAIN.controller.vHome
		 */
	});
});