sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"ZCA/ZCA_BP_MAINTAIN/controller/Utility"
], function (Controller, Utility) {
	"use strict";

	return Controller.extend("ZCA.ZCA_BP_MAINTAIN.controller.vGeneralAddress", {

		/**
		 * Called when a controller is instantiated and its View controls (if available) are already created.
		 * Can be used to modify the View before it is displayed, to bind event handlers and do other one-time initialization.
		 * @memberOf ZCA.ZCA_BP_MAINTAIN.view.vGeneralAddress
		 */

		that: this,

		onInit: function () {

		},

		// Postal Code
		onChangePC: function (oEvent) {
			var oInput = oEvent.getSource();
			var sValue = oEvent.getParameter("newValue");
			// if (!sValue) {
			// 	oInput.setValueState(sap.ui.core.ValueState.Error);
			// 	oInput.setValueStateText("* Enter a value");
			// } else {
			// 	oInput.setValueState(sap.ui.core.ValueState.None);
			// }

			oInput.setValue(sValue.toUpperCase());
		},

		// Street Name
		onChangeStreet: function (oEvent) {
			var oInput = oEvent.getSource();
			var sValue = oEvent.getParameter("newValue");
			if (!sValue) {
				oInput.setValueState(sap.ui.core.ValueState.Error);
				oInput.setValueStateText(Utility.msgMandatory);
			} else {
				oInput.setValueState(sap.ui.core.ValueState.None);
			}
		},

		// City
		onChangeCity: function (oEvent) {
			var oInput = oEvent.getSource();
			var sValue = oEvent.getParameter("newValue");
			if (!sValue) {
				oInput.setValueState(sap.ui.core.ValueState.Error);
				oInput.setValueStateText(Utility.msgMandatory);
			} else {
				oInput.setValueState(sap.ui.core.ValueState.None);
			}
		},

		// Country Select
		onCountrySelect: function (oEvent) {
			var oCbox = oEvent.getSource();
			var sItem = oCbox.getSelectedItem();

			if (!sItem) {
				oCbox.setValueState(sap.ui.core.ValueState.Error);
				oCbox.setValueStateText(Utility.msgChooseValue);
				return;
			}
			// Not blank	
			else {
				oCbox.setValueState(sap.ui.core.ValueState.None);
				// Filter Regions	
				var oView = oEvent.getSource().getParent().getParent().getParent().getParent().getParent().getParent();
				var oRegionsCbox = oView.byId("idcbRegion");
				var oBindings = oRegionsCbox.getBinding("items");
				var vCountry = sItem.getProperty("key");
				oBindings.filter(new sap.ui.model.Filter("Country", sap.ui.model.FilterOperator.EQ, vCountry), true);
				// 
				// set Address fields mandatory
				var adrModel = this.getOwnerComponent().getModel("adrModel");
				var i = 0;
				for (; adrModel[i];) {
					if (adrModel[i].CountryKey === vCountry) {
						var uiModel = this.getOwnerComponent().getModel("uiModel");
						var uiData = uiModel.getProperty("/");
						uiData.RequiredDistrict = adrModel[i].DistrictRequired;
						uiData.RequiredPOBox = adrModel[i].POBoxRequired;
						uiData.RequiredPostalcode = adrModel[i].PostalCodeRequired;
						uiData.RequiredRegion = adrModel[i].RegionRequired;
						i = adrModel.length;
						uiModel.refresh();
					}
					i++;
				}
			}
		},

		/**
		 * Similar to onAfterRendering, but this hook is invoked before the controller's View is re-rendered
		 * (NOT before the first rendering! onInit() is used for that one!).
		 * @memberOf ZCA.ZCA_BP_MAINTAIN.view.vGeneralAddress
		 */
		onBeforeRendering: function () {
			Utility._vGeneralAddress = this;
			// Prepare path
			// var vPath = "/A_Details(RequestNumber=" +
			// 	Utility._selectedRequest.RequestNumber +
			// 	",PartnerType='" + Utility._partnerType + "',PartnerSeqNum=" +
			// 	Utility._partnerSeqNum + ")";

			// // Bind model path to view
			// this.getView().bindElement({
			// 	path: vPath,
			// 	model: "cModel"
			// });
		}

		/**
		 * Called when the View has been rendered (so its HTML is part of the document). Post-rendering manipulations of the HTML could be done here.
		 * This hook is the same one that SAPUI5 controls get after being rendered.
		 * @memberOf ZCA.ZCA_BP_MAINTAIN.view.vGeneralAddress
		 */
		//	onAfterRendering: function() {
		//
		//	},

		/**
		 * Called when the Controller is destroyed. Use this one to free resources and finalize activities.
		 * @memberOf ZCA.ZCA_BP_MAINTAIN.view.vGeneralAddress
		 */
		//	onExit: function() {
		//
		//	}

	});

});