sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"sap/m/MessageToast",
	"ZCA/ZCA_BP_MAINTAIN/controller/Utility"
], function (Controller, MToast, Utility) {
	"use strict";

	return Controller.extend("ZCA.ZCA_BP_MAINTAIN.controller.vDetail", {

		/**
		 * Called when a controller is instantiated and its View controls (if available) are already created.
		 * Can be used to modify the View before it is displayed, to bind event handlers and do other one-time initialization.
		 * @memberOf ZCA.ZCA_BP_MAINTAIN.view.vDetail
		 */

		utility: Utility,
		hasCPError: false,
		hasBTPError: false,
		totalHttpRequests: 0,
		totalHttpResponses: 0,
		totalHttpRequestsU: 0,
		totalHttpResponsesU: 0,
		totalDelRequests: 0,
		totalDelResponses: 0,
		//
		//
		onInit: function () {
			//
			// this.getView().addStyleClass(this.getOwnerComponent().getContentDensityClass());

			this._Router = sap.ui.core.UIComponent.getRouterFor(this);
		},

		/**
		 * Similar to onAfterRendering, but this hook is invoked before the controller's View is re-rendered
		 * (NOT before the first rendering! onInit() is used for that one!).
		 * @memberOf ZCA.ZCA_BP_MAINTAIN.view.vDetail
		 */
		onBeforeRendering: function () {
			Utility._vDetail = this;
			Utility.setViewUIbindings();
		},

		/**
		 * Called when the View has been rendered (so its HTML is part of the document). Post-rendering manipulations of the HTML could be done here.
		 * This hook is the same one that SAPUI5 controls get after being rendered.
		 * @memberOf ZCA.ZCA_BP_MAINTAIN.view.vDetail
		 */
		// onAfterRendering: function () {
		// }

		/**
		 * Called when the Controller is destroyed. Use this one to free resources and finalize activities.
		 * @memberOf ZCA.ZCA_BP_MAINTAIN.view.vDetail
		 */
		//	onExit: function() {
		//
		//	}F

		// Hide / Unhide BTP section
		handleBTPSwitch: function (oEvent) {
			var oParams = oEvent.getParameters();
			this.getView().byId("idsBTP").setVisible(oParams.state);
		},

		// Hide / Unhide Payer section
		handlePayerSwitch: function (oEvent) {
			var oParams = oEvent.getParameters();
			this.getView().byId("idsPayer").setVisible(oParams.state);
		},

		// Hide / Unhide Contacts section
		handleContactSwitch: function (oEvent) {
			var oParams = oEvent.getParameters();
			this.getView().byId("idsContact").setVisible(oParams.state);
		},

		onEdit: function () {
			switch (Utility._applMode) {
			case Utility.Edit:
				Utility._applMode = Utility.Display;
				break;
			case Utility.Display:
				Utility._applMode = Utility.Edit;
				break;
			}
			// 
			Utility.setUIConfigs(this);
		},
		//
		/*  If any of Ad Sales or Content or Branded are selected, then Others should be switched off. 
			If Others is selected, then all other switches must be switched off. 
			If Ad Sales is selected, then show Agency Customer switch. */

		handleAddlDataSwitch: function (oEvent) {
			var uiData = this.getOwnerComponent().getModel("uiModel").getProperty("/");
			if (uiData.ContentSales === false &&
				uiData.AdSales === false &&
				uiData.ChannelSales === false &&
				uiData.AgencyCustomer === false &&
				uiData.ProformaCustomer === false) {
				uiData.NotRelevant = oEvent.getParameters().state ? false : true;
			} else {
				uiData.NotRelevant = oEvent.getParameters().state ? false : false;
			}
			// 
			if (uiData.AdSales === false) {
				uiData.AgencyCustomer = false;
			}
		},
		// If Others is selected, then all other switches must be switched off.
		handleNotRelevantSwitch: function (oEvent) {
			var uiData = this.getOwnerComponent().getModel("uiModel").getProperty("/");
			if (oEvent.getParameters().state) {
				uiData.ContentSales = false;
				uiData.AdSales = false;
				uiData.ChannelSales = false;
				uiData.AgencyCustomer = false;
				uiData.ProformaCustomer = false;
			}
		},

		// onBack
		onBack: function () {
			var xsModel = this.getOwnerComponent().getModel("xsModel");
			xsModel.refresh();
			this._Router.navTo("vHome");
		},

		// Save Request
		onSave: function () {

			// this.startBPCreation();

			// this.createBusinessPartner();

			this._saveRequest(Utility.actionSave);
		},

		// Delete Request
		onDelete: function () {
			// if (Utility.reqType === Utility.reqTypeNew) {
			// 	this._Router.navTo("vHome");
			// 	Utility.setLocalModelCreate();
			// 	MToast.show("Draft request deleted successfully.");
			// } else {
			// MToast.show("Functionality coming soon..!!");
			// }
			// this._saveRequest(Utility.actionDelete);
			this._deleteRequest();
		},

		// Delete Request 
		_deleteRequest: function () {

			var i = 0;
			var lModel = this.getOwnerComponent().getModel("lModel");
			var xsModel = this.getOwnerComponent().getModel("xsModel");

			var vPath = "/A_Details(RequestNumber=" + Utility._selectedRequest.RequestNumber + ",PartnerType='',PartnerSeqNum=0)";
			this._deleteEntity(xsModel, vPath);
			// Delete partners 
			var aPartners = lModel.getProperty("/Partners");
			for (; aPartners[i];) {
				vPath = "/A_Details(RequestNumber=" + Utility._selectedRequest.RequestNumber + ",PartnerType='" +
					aPartners[i].PartnerType + "',PartnerSeqNum=" + aPartners[i].PartnerSeqNum + ")";
				this._deleteEntity(xsModel, vPath);
				i++;
			}
			//
			i = 0;
			var aSalesArea = lModel.getProperty("/SalesArea");
			for (; aSalesArea[i];) {
				vPath = "/A_SalesArea(RequestNumber=" + Utility._selectedRequest.RequestNumber + ",SalesOrg='" +
					aSalesArea[i].SalesOrg + "')";
				this._deleteEntity(xsModel, vPath);
				i++;
			}
			//
			i = 0;
			var aTaxData = lModel.getProperty("/TaxData");
			for (; aTaxData[i];) {
				vPath = "/A_TaxData(RequestNumber=" + Utility._selectedRequest.RequestNumber + ",TaxNumType='" +
					aTaxData[i].TaxNumType + "')";
				this._deleteEntity(xsModel, vPath);
				i++;
			}
			//
			i = 0;
			var aComm = lModel.getProperty("/Communication");
			for (; aComm[i];) {
				vPath = "/A_Communication(RequestNumber=" + Utility._selectedRequest.RequestNumber + ",PartnerType='" +
					aComm[i].PartnerType + "',PartnerSeqNum=" + aComm[i].PartnerSeqNum + ",CommType='" +
					aComm[i].CommType + "',CommSeqNumber=" + aComm[i].CommSeqNumber + ")";
				this._deleteEntity(xsModel, vPath);
				i++;
			}
			//
			i = 0;
			var aCollections = lModel.getProperty("/Collections");
			for (; aCollections[i];) {
				vPath = "/A_Collections(RequestNumber=" + Utility._selectedRequest.RequestNumber + ",CollSegment='" +
					aCollections[i].CollSegment + "')";
				this._deleteEntity(xsModel, vPath);
				i++;
			}
			//
			i = 0;
			var aHistory = lModel.getProperty("/History");
			for (; aHistory[i];) {
				vPath = "/A_History(RequestNumber=" + Utility._selectedRequest.RequestNumber + ",PartnerType='" +
					aHistory[i].PartnerType + "',PartnerSeqNum=" + aHistory[i].PartnerSeqNum + ",uuid='" +
					aHistory[i].uuid + "')";
				this._deleteEntity(xsModel, vPath);
				i++;
			}
		},

		// Delete Entity
		_deleteEntity: function (oModel, sPath) {
			this.totalDelRequests += 1;
			var that = this;
			oModel.remove(sPath, {
				success: function (oData, oResponse) {
					that.totalDelResponses += 1;
					that._checkAllDeleted();
				}
			});
		},

		_checkAllDeleted: function () {
			if (this.totalDelRequests === this.totalDelResponses) {
				var xsModel = this.getOwnerComponent().getModel("xsModel");
				var vPath = "/A_Header(" + Utility._selectedRequest.RequestNumber + ")";
				var that = this;
				xsModel.remove(vPath, {
					success: function (oData, oResponse) {
						var message = "Request Number " + Utility._selectedRequest.RequestNumber + " Deleted";
						MToast.show(message);
						that._Router.navTo("vHome");
					}
				});
			}
		},

		// Submit Request
		onSubmit: function () {
			this._saveRequest(Utility.actionSubmit);
		},

		// Approve Request
		onApprove: function () {
			if (Utility._userRole === Utility.Validator) {
				this._saveRequest(Utility.actionApprove);
			}
			// Credit Control 
			else {
				// Create Business Partner in Backend 
				this.startBPCreation();
				// Update other BP details in Backend 
				// this.createBPadditionalData();
				// Update cloud DB 
				// this._saveRequest(Utility.actionApprove);
			}
		},

		// Reject Request
		onReject: function () {
			this._saveRequest(Utility.actionReject);
		},

		_saveRequest: function (vAction) {

			Utility._date = new Date();

			var lModel = this.getOwnerComponent().getModel("lModel");
			var xsModel = this.getOwnerComponent().getModel("xsModel");
			// TO DO
			// var that = this;

			// xsModel.attachBatchRequestCompleted(function () {
			// 	that.collectAllSuccessRequests();
			// });

			// xsModel.attachBatchRequestFailed(function () {
			// 	that.collectAllSuccessRequests();
			// });

			// xsModel.attachRequestCompleted(function () {
			// 	that.collectAllSuccessRequests();
			// });

			// xsModel.attachRequestFailed(function () {
			// 	that.collectAllSuccessRequests();
			// });

			// TO DO - delete un-saved request

			var aHeader = this._maintainHeader(vAction, xsModel, lModel);
			this._maintainDetails(vAction, xsModel, lModel, aHeader);

			if (vAction !== Utility.actionReject) {
				if (Utility._userRole === Utility.Requester || ((Utility._userRole === Utility.Validator || Utility._userRole === Utility.CreditControl) &&
						// TO DO - change flag
						Utility.changesMadeAfterSave === false)) {
					// Not credit control
					if (Utility._userRole !== Utility.CreditControl) {
						this._maintainComm(vAction, xsModel, lModel, aHeader);
						this._maintainSalesArea(vAction, xsModel, lModel, aHeader);
						this._maintainTaxData(vAction, xsModel, lModel, aHeader);
					}

					// only Credit Control
					this._maintainCollections(vAction, xsModel, lModel, aHeader);
				}
			}
			//
			// var that = this;
			// xsModel.submitChanges({
			// 	success: function (oEvent) {
			// 		Utility._selectedRequest = aHeader;
			// 		if (vAction === Utility.actionSubmit || vAction === Utility.actionApprove) {
			// 			Utility._applMode = Utility.Display;
			// 			Utility.setUIConfigs(that);
			// 		}

			// 		Utility.reqType = Utility.reqTypeUpdate; // Convert the saved request to Update mode
			// 		MToast.show("Request processed successfully");
			// 	},
			// 	error: function (oEvent) {
			// 		MToast.show("Request failed, please try again");
			// 	}
			// });
		},

		// 
		_maintainHeader: function (vAction, oModel, pModel) {
			var aHeader = {};
			var path = "/A_Header";
			var aDetails = pModel.getProperty("/BP_Details");

			var vhCountry = Utility._Component.getModel("vhCountry");
			var cPath = "/ZCA_C_Country('" + aDetails.CountryKey + "')";
			aHeader.CountryName = vhCountry.getProperty(cPath).CountryName;
			aHeader.CustName1 = aDetails.CustName1;
			aHeader.CountryKey = aDetails.CountryKey;
			aHeader.BPNumber = aDetails.PartnerNumber;

			switch (vAction) {
			case Utility.actionSave:
				aHeader.ReqStatus = Utility.keyReqStatusDraft;
				aHeader.StatusRole = Utility._userRole;
				break;
			case Utility.actionSubmit:
				aHeader.ReqStatus = Utility.keyReqStatusValidating;
				aHeader.StatusRole = Utility.Validator;
				break;
			case Utility.actionApprove:
				if (Utility._userRole === Utility.Validator) {
					aHeader.ReqStatus = Utility.keyReqStatusCreditChecking;
					aHeader.StatusRole = Utility.CreditControl;
				} else {
					aHeader.ReqStatus = Utility.keyReqStatusApproved;
					aHeader.StatusRole = Utility._userRole;
				}
				break;
			case Utility.actionReject:
				aHeader.ReqStatus = Utility.keyReqStatusRejected;
				if (Utility._userRole === Utility.Validator) {
					aHeader.StatusRole = Utility.Validator;
				} else {
					aHeader.StatusRole = Utility.CreditControl;
				}
				break;
			case Utility.actionDelete:
				path = path + "(" + aHeader.RequestNumber + ")";
				oModel.remove(path, aHeader);
				break;
			default:
				aHeader.ReqStatus = Utility.keyReqStatusDraft;
				aHeader.StatusRole = Utility._userRole;
			}

			// var that = this;
			if (Utility.reqType === Utility.reqTypeNew) {
				aHeader.RequestNumber = Utility._newRequest;
				// aHeader.CreatedOn = Utility._date.getUTCDate();
				// aHeader.CreatedAt = Utility._date.getTime();
				aHeader.CreatedTstamp =
					// sap.ui.model.odata.ODataUtils.formatValue(new Date(), "Edm.DateTime");
					new Date();
				aHeader.CreatedBy = Utility.userModel.getProperty("/").name.toUpperCase();
				//
				this.createEntity(oModel, aHeader, path, vAction, aHeader);

			} else if (Utility.reqType === Utility.reqTypeUpdate) {

				aHeader.RequestNumber = Utility._selectedRequest.RequestNumber;
				aHeader.CreatedTstamp = Utility._selectedRequest.CreatedTstamp;
				aHeader.CreatedBy = Utility._selectedRequest.CreatedBy;
				// aHeader.BPNumber = Utility._selectedRequest.BPNumber;

				path = path + "(" + aHeader.RequestNumber + ")";

				// oModel.update(path, aHeader);
				this.updateEntity(oModel, aHeader, path, vAction, aHeader);
			}

			Utility.reqHeader = aHeader;
			return aHeader;
		},

		// CREATE
		createEntity: function (oModel, vData, vPath, vAction, vHeader) {

			this.totalHttpRequests += 1;
			var that = this;
			oModel.create(vPath, vData, {
				success: function (oData, oResponse) {
					that.collectHttpResponses(vAction, vHeader, oResponse);
				},
				error: function (oError) {
					// TO DO
				}
			});
		},

		// UPDATE
		updateEntity: function (oModel, vData, vPath, vAction, vHeader) {

			this.totalHttpRequestsU += 1;
			var that = this;
			oModel.update(vPath, vData, {
				success: function (oData, oResponse) {
					that.collectHttpResponsesU(vAction, vHeader, oResponse);
				},
				error: function (oError) {
					that.messageRequestFailed(oError);
				}
			});
		},
		// Collect Create Requests
		collectHttpResponses: function (vAction, aHeader, oResponse) {
			this.totalHttpResponses += 1;
			if ((this.totalHttpRequests + this.totalHttpRequestsU) === (this.totalHttpResponses + this.totalHttpResponsesU)) {
				Utility._selectedRequest = aHeader;
				if (vAction === Utility.actionSubmit || vAction === Utility.actionApprove || vAction === Utility.actionReject) {
					Utility._applMode = Utility.Display;
					Utility.setUIConfigs(this);
				}

				Utility.reqType = Utility.reqTypeUpdate; // Convert the saved request to Update mode
				Utility._newRequest += 1; // For sequential create within the session
				MToast.show("Request saved");

				// Trigger Email 
				this.sendEmail(vAction, aHeader);
			}
		},
		// Collect Update Requests
		collectHttpResponsesU: function (vAction, aHeader, oResponse) {
			this.totalHttpResponsesU += 1;
			if ((this.totalHttpRequests + this.totalHttpRequestsU) === (this.totalHttpResponses + this.totalHttpResponsesU)) {
				Utility._selectedRequest = aHeader;
				if (vAction === Utility.actionSubmit || vAction === Utility.actionApprove || vAction === Utility.actionReject) {
					Utility._applMode = Utility.Display;
					Utility.setUIConfigs(this);
				}

				Utility.reqType = Utility.reqTypeUpdate; // Convert the saved request to Update mode
				Utility._newRequest += 1; // For sequential create within the session
				MToast.show("Request updated successfully");

				// Trigger Email 
				this.sendEmail(vAction, aHeader);
			}
		},
		//
		messageRequestFailed: function (oError) {
			MToast.show("Could not process the request");
		},

		// 
		_maintainDetails: function (vAction, oModel, pModel, vHeader) {

			var aDetail = pModel.getProperty("/BP_Details");
			var aPartners = pModel.getProperty("/Partners");
			var i = 0;
			var sPath;
			var aHistory = {};
			var dPath = "/A_Details";
			var hPath = "/A_History";

			// var bHistory = {};

			aDetail.RequestNumber = vHeader.RequestNumber;
			aDetail.PartnerType = Utility.keyNoPartner; // ''
			aDetail.PartnerSeqNum = 0;

			// Additional Attributes
			var uiModel = Utility._Component.getModel("uiModel");
			var uiModelData = uiModel.getProperty("/");

			if (uiModelData.AdSales) {
				aDetail.AdSales = Utility.keyAdSales;
			} else {
				aDetail.AdSales = null;
			}

			// Channel Sales
			if (uiModelData.ChannelSales) {
				aDetail.ChannelSales = Utility.keyChannelSales;
			} else {
				aDetail.ChannelSales = null;
			}

			// Content Sales
			if (uiModelData.ContentSales) {
				aDetail.ContentSales = Utility.keyContentSales;
			} else {
				aDetail.ContentSales = null;
			}

			// Agency Customer
			if (uiModelData.AgencyCustomer) {
				aDetail.AgencyCustomer = Utility.keyAgencyCustomer;
			} else {
				aDetail.AgencyCustomer = null;
			}

			// Proforma Customer
			if (uiModelData.ProformaCustomer) {
				aDetail.ProformaCustomer = Utility.keyProformaCustomer;
			} else {
				aDetail.ProformaCustomer = null;
			}

			// Not Relevant
			if (uiModelData.NotRelevant) {
				aDetail.NotRelevant = Utility.keyNotRelevant;
			} else {
				aDetail.NotRelevant = null;
			}

			// Email
			if (uiModelData.StateEmail === true) {
				aDetail.PreferredCommType = Utility.keyDefaultEmail;
			}
			// MobilePhone
			if (uiModelData.StateMob === true) {
				aDetail.PreferredCommType = Utility.keyMob;
			}
			// Telephone
			if (uiModelData.StateTel === true) {
				aDetail.PreferredCommType = Utility.keyTel;
			}

			// Populate Customer Grouping 
			aDetail.CustGrouping = (aDetail.CustGrouping === undefined) ? "ZCU3" : aDetail.CustGrouping;

			//
			aHistory.RequestNumber = vHeader.RequestNumber;
			aHistory.PartnerType = aDetail.PartnerType;
			aHistory.PartnerSeqNum = aDetail.PartnerSeqNum;
			aHistory.ChangedRole = Utility._userRole;
			aHistory.uuid = Utility.getUUID();

			// aHistory.ChangedOn = Utility._date.getDate();
			// aHistory.ChangedAt = Utility._date.getTime();

			aHistory.ChangedTStamp =
				// 	sap.ui.model.odata.ODataUtils.formatValue(new Date(), "Edm.DateTime");
				new Date();
			// aHistory.ChangedBy = Utility.userModel.getProperty("/").name.toUpperCase();
			aHistory.ChangedBy = Utility.userModel.getProperty("/").displayName;
			aHistory.Comments = aDetail.Comments;

			if (vAction !== Utility.actionDelete) {
				if (Utility.reqType === Utility.reqTypeNew) {
					// CREATE

					// oModel.create(dPath, aDetail);
					this.createEntity(oModel, aDetail, dPath, vAction, vHeader);
					this.createEntity(oModel, aHistory, hPath, vAction, vHeader);
					// oModel.create(hPath, aHistory);

					//
					for (; aPartners[i];) {
						// oModel.create(dPath, aPartners[i]);
						aPartners[i].AdSales = aDetail.AdSales;
						aPartners[i].ContentSales = aDetail.ContentSales;
						aPartners[i].ChannelSales = aDetail.ChannelSales;

						this.createEntity(oModel, aPartners[i], dPath, vAction, vHeader);

						// bHistory = {};
						// bHistory.RequestNumber = vHeader.RequestNumber;
						// bHistory.PartnerType = aPartners[i].PartnerType;
						// bHistory.PartnerSeqNum = aPartners[i].PartnerSeqNum;
						// bHistory.ChangedTStamp = new Date();
						// bHistory.ChangedBy = "DAMALCW1";
						// bHistory.Comments = aDetail.Comments;

						// oModel.create(hPath, bHistory);
						i++;
					}
				}
				// UPDATE
				else {

					sPath = dPath + "(RequestNumber=" + aDetail.RequestNumber + ",PartnerType='',PartnerSeqNum=0)";
					this.updateEntity(oModel, aDetail, sPath, vAction, vHeader);

					// oModel.update(sPath, aDetail);

					this.createEntity(oModel, aHistory, hPath, vAction, vHeader);
					// oModel.create(hPath, aHistory); // BP
					//
					for (; aPartners[i];) {
						sPath = dPath + "(RequestNumber=" + aDetail.RequestNumber + ",PartnerType='" +
							aPartners[i].PartnerType + "',PartnerSeqNum=" + aPartners[i].PartnerSeqNum + ")";

						this.updateEntity(oModel, aPartners[i], sPath, vAction, vHeader);
						// oModel.update(sPath, aPartners[i]);
						//						

						// aHistory.PartnerType = aPartners[i].PartnerType;
						// aHistory.PartnerSeqNum = aPartners[i].PartnerSeqNum;
						// oModel.create(hPath, aHistory); // Partners
						//
						i++;
					}
				}
			}
			// DELETE
			else {
				if (Utility.reqType === Utility.reqTypeUpdate) {
					sPath = dPath + "(RequestNumber=" + aDetail.RequestNumber + ")";
					oModel.remove(sPath);
					//
					hPath = hPath + "(RequestNumber=" + aDetail.RequestNumber + ")";
					oModel.remove(hPath);
				}
			}
		},

		// 
		_maintainComm: function (vAction, oModel, pModel, vHeader) {

			var aComm1 = pModel.getProperty("/Communication");
			var aComm = JSON.parse(JSON.stringify(aComm1));
			var i = 0;
			var sPath;
			var path = "/A_Communication";

			if (vAction !== Utility.actionDelete) {
				// CREATE
				if (Utility.reqType === Utility.reqTypeNew) {

					i = 0;
					for (; aComm[i];) {
						// oModel.create(path, aComm[i]);

						aComm[i].DefaultComm = (aComm[i].DefaultComm === true) ? "true" : "false";
						this.createEntity(oModel, aComm[i], path, vAction, vHeader);
						i++;
					}
				}
				// UPDATE
				else {

					i = 0;
					for (; aComm[i];) {
						sPath = path + "(RequestNumber=" + vHeader.RequestNumber + ",PartnerType='" +
							aComm[i].PartnerType + "',PartnerSeqNum=" + aComm[i].PartnerSeqNum + ",CommType='" +
							aComm[i].CommType + "',CommSeqNumber=" + aComm[i].CommSeqNumber + ")";

						aComm[i].DefaultComm = (aComm[i].DefaultComm === true) ? "true" : "false";

						this.updateEntity(oModel, aComm[i], sPath, vAction, vHeader);
						// oModel.update(sPath, aComm[i]);
						i++;
					}
				}
			}
			// DELETE
			else {
				if (Utility.reqType === Utility.reqTypeUpdate) {
					sPath = path + "(RequestNumber=" + vHeader.RequestNumber + ")";
					oModel.remove(sPath);
				}
			}
		},

		// 
		_maintainSalesArea: function (vAction, oModel, pModel, vHeader) {

			var aSalesArea = pModel.getProperty("/SalesArea");
			var i = 0;
			var sPath;
			var path = "/A_SalesArea";

			if (vAction !== Utility.actionDelete) {
				// CREATE
				if (Utility.reqType === Utility.reqTypeNew) {
					//
					i = 0;
					for (; aSalesArea[i];) {
						// oModel.create(path, aSalesArea[i]);
						this.createEntity(oModel, aSalesArea[i], path, vAction, vHeader);
						i++;
					}
				}
				// UPDATE
				else {

					i = 0;
					for (; aSalesArea[i];) {
						sPath = path + "(RequestNumber=" + aSalesArea[i].RequestNumber + ",SalesOrg='" +
							aSalesArea[i].SalesOrg + "')";

						this.updateEntity(oModel, aSalesArea[i], sPath, vAction, vHeader);
						// oModel.update(sPath, aSalesArea[i]);
						i++;
					}
				}
			}
			// DELETE
			else {
				if (Utility.reqType === Utility.reqTypeUpdate) {
					sPath = path + "(RequestNumber=" + vHeader.RequestNumber + ")";
					oModel.remove(sPath);
				}
			}
		},

		// 
		_maintainTaxData: function (vAction, oModel, pModel, vHeader) {

			var aTaxData = pModel.getProperty("/TaxData");
			var i = 0;
			var sPath;
			var path = "/A_TaxData";

			if (vAction !== Utility.actionDelete) {
				// CREATE
				if (Utility.reqType === Utility.reqTypeNew) {
					//
					i = 0;
					for (; aTaxData[i];) {
						// oModel.create(path, aTaxData[i]);
						this.createEntity(oModel, aTaxData[i], path, vAction, vHeader);
						i++;
					}
				}
				// UPDATE
				else {

					i = 0;
					for (; aTaxData[i];) {
						sPath = path + "(RequestNumber=" + aTaxData[i].RequestNumber + ",TaxNumType='" +
							aTaxData[i].TaxNumType + "')";

						this.updateEntity(oModel, aTaxData[i], sPath, vAction, vHeader);
						// oModel.update(sPath, aTaxData[i]);
						i++;
					}
				}
			}
			// DELETE
			else {
				if (Utility.reqType === Utility.reqTypeUpdate) {
					sPath = path + "(RequestNumber=" + vHeader.RequestNumber + ")";
					oModel.remove(sPath);
				}
			}
		},

		// 
		_maintainCollections: function (vAction, oModel, pModel, vHeader) {

			var aCollections = pModel.getProperty("/Collections");
			var i = 0;
			var sPath;
			var path = "/A_Collections";

			if (vAction !== Utility.actionDelete) {
				// CREATE
				if (Utility.reqType === Utility.reqTypeNew) {
					//
					for (; aCollections[i];) {
						aCollections[i].RequestNumber = vHeader.RequestNumber;
						// oModel.create(path, aCollections[i]);
						this.createEntity(oModel, aCollections[i], path, vAction, vHeader);
						i++;
					}
				}
				// UPDATE
				else {
					i = 0;
					for (; aCollections[i];) {
						sPath = path + "(RequestNumber=" + aCollections[i].RequestNumber + ",CollSegment='" +
							aCollections[i].CollSegment + "')";

						this.updateEntity(oModel, aCollections[i], sPath, vAction, vHeader);
						// oModel.update(sPath, aCollections[i]);
						i++;
					}
				}
			}
			// DELETE
			else {
				if (Utility.reqType === Utility.reqTypeUpdate) {
					sPath = path + "(RequestNumber=" + vHeader.RequestNumber + ")";
					oModel.remove(sPath);
				}
			}
		},

		/// **********************************************************
		startBPCreation: function () {
			var lModel = this.getOwnerComponent().getModel("lModel");
			var apiModel = this.getOwnerComponent().getModel("apiBP");

			var aPartners = lModel.getProperty("/Partners");

			this._indexBTP = 0;
			this._indexCP = 0;
			this.hasBTPError = false;
			this.hasCPError = false;
			this.totalPartners = aPartners.length;

			if (aPartners.length > 0) {
				//
				apiModel.setUseBatch(true);
				//
				this.createBTPs(lModel, apiModel);
				//
				this.createCPs(lModel, apiModel);
			}
			// Cerate BP alone
			else {
				this.createBusinessPartner(lModel, apiModel);
			}
		},

		/// **********************************************************
		createBusinessPartner: function () {

			var lModel = this.getOwnerComponent().getModel("lModel");
			var apiModel = this.getOwnerComponent().getModel("apiBP");

			var aDetails = lModel.getProperty("/BP_Details"),
				aPartners = lModel.getProperty("/Partners"),
				aComm = lModel.getProperty("/Communication"),
				aSales = lModel.getProperty("/SalesArea"),
				aTax = lModel.getProperty("/TaxData");
			// aCollections = lModel.getProperty("/Collections");

			var jModel = new sap.ui.model.json.JSONModel("jModel");
			jModel.setDefaultBindingMode("OneWay");

			// Header info
			var vHeader = {
				BusinessPartnerCategory: "2", // Org 1-Person 
				BusinessPartnerGrouping: aDetails.CustGrouping,
				// AuthorizationGroup: aDetails.CustGrouping,
				OrganizationBPName1: aDetails.CustName1,
				OrganizationBPName2: aDetails.CustName2,
				OrganizationBPName3: aDetails.CustName3,
				OrganizationBPName4: aDetails.CustName4,
				SearchTerm1: aDetails.SearchTerm1,
				FormOfAddress: "0003", // Company
				Language: "EN"
			};

			jModel.setProperty("/", vHeader);
			var vAddress = [];

			// Address
			var address = {
				HouseNumber: aDetails.HouseNumber,
				StreetName: aDetails.StreetName,
				StreetPrefixName: aDetails.Street2,
				AdditionalStreetPrefixName: aDetails.Street3,
				StreetSuffixName: aDetails.Street4,
				District: aDetails.District,
				PostalCode: aDetails.PostalCode,
				Region: aDetails.RegionState,
				CityName: aDetails.City,
				Country: aDetails.CountryKey,

				POBox: aDetails.POBoxNumber,
				Language: "EN"

				// PrfrdCommMediumType: aDetails.PreferredCommType
			};

			// Communication - Telephone 
			var i = 0;

			// 
			address.to_PhoneNumber = [];
			address.to_MobilePhoneNumber = [];

			for (; aComm[i];) {
				if (aComm[i].PartnerType === Utility.keyNoPartner) {
					switch (aComm[i].CommType) {
					case Utility.keyTel:
						{
							var vTel = {
								PhoneNumber: aComm[i].CommValue
							};

							address.to_PhoneNumber.push(vTel);
							break;
						}
					case Utility.keyMob:
						{
							var vMob = {
								PhoneNumber: aComm[i].CommValue
							};

							address.to_MobilePhoneNumber.push(vMob);
							break;
						}
					}
				}
				i++;
			}
			//
			vAddress.push(address);
			jModel.setProperty("/to_BusinessPartnerAddress", vAddress);
			//
			// Business Partner Role 
			var vRoles = [{
				"BusinessPartnerRole": "FLCU01" // Customer
			}, {
				"BusinessPartnerRole": "FLCU00" // FI Customer 
			}, {
				"BusinessPartnerRole": "UDM000" // BP Collections Management
			}, {
				"BusinessPartnerRole": "UKM000" // SAP Credit Management
			}];

			jModel.setProperty("/to_BusinessPartnerRole", vRoles);

			/*  Tax Data */
			i = 0;
			var vTaxData = [],
				vTax;
			for (; aTax[i];) {
				vTax = {
					BPTaxType: aTax[i].TaxNumType,
					BPTaxNumber: aTax[i].TaxNumber
				};

				vTaxData.push(vTax);
				i++;
			}
			if (vTaxData.length > 0) {
				jModel.setProperty("/to_BusinessPartnerTax", vTaxData);
			}
			// 
			// BP Contact 
			i = 0;
			var bpContacts = [];
			for (; aPartners[i];) {
				if (aPartners[i].PartnerType === Utility.keyContact) {
					var bpContact = {
						IsStandardRelationship: true,
						BusinessPartnerPerson: aPartners[i].PartnerNumber, // aPartners[i].PartnerNumber
						RelationshipCategory: "BUR001" // Has contact person 	
					};

					bpContact.to_ContactRelationship = {
						ContactPersonDepartment: "0009", // Finance

						// PhoneNumber: "9876541230",
						// EmailAddress: "contactemail@bbc.com",

						RelationshipCategory: "BUR001" // Is contact person for 	
					};
					bpContacts.push(bpContact);

				}
				i++;
			}
			if (bpContacts.length > 0) {
				jModel.setProperty("/to_BusinessPartnerContact", bpContacts);
			}

			// 
			// Customer 
			i = 0;
			var bpSalesOrgs = [];
			for (; aSales[i];) {

				var bpSalesOrg = {

					SalesOrganization: aSales[i].SalesOrg,
					DistributionChannel: "00",
					Division: "00",
					Currency: aSales[i].CurrencyKey,
					CustomerPaymentTerms: aDetails.PaymentTerms,
					SalesOffice: aSales[i].SalesOffice,
					SalesDistrict: aSales[i].SalesDistrict,
					CustomerPricingProcedure: "1"
				};

				// Partner Functions
				var bpPartFuns = [{
					DistributionChannel: "00",
					Division: "00",
					PartnerFunction: "BP"
				}, {
					DistributionChannel: "00",
					Division: "00",
					PartnerFunction: "SP"
				}, {
					DistributionChannel: "00",
					Division: "00",
					PartnerFunction: "SH"
				}, {
					DistributionChannel: "00",
					Division: "00",
					PartnerFunction: "PY"
				}];

				// Additional BTPs
				var j = 0;
				for (; aPartners[j];) {
					if (aPartners[j].PartnerType === Utility.keyBTP) {
						var bpBTP = {
							DistributionChannel: "00",
							Division: "00",
							PartnerFunction: "BP",
							BPCustomerNumber: aPartners[j].PartnerNumber
						};

						bpPartFuns.push(bpBTP);
					}
					j++;
				}

				bpSalesOrg.to_PartnerFunction = bpPartFuns;

				bpSalesOrgs.push(bpSalesOrg);
				i++;
			}

			if (bpSalesOrgs.length > 0) {
				jModel.setProperty("/to_Customer", {});
				jModel.setProperty("/to_Customer/to_CustomerSalesArea", bpSalesOrgs);
			}

			// Get Hierarchy Data
			var jData = jModel.getProperty("/");

			// apiModel.getSecurityToken();

			var that = this;
			apiModel.create("/A_BusinessPartner", jData, {
				success: function (oData, response) {

					var message = "Business Partner " + oData.BusinessPartner + " Created";
					MToast.show(message);

					that.updateBPAdditionalInfo(oData, response, null);
				},
				error: function (oError) {
					that.updateBPErrorLog(oError);
				}
			});
		},

		// Additional Information
		updateBPAdditionalInfo: function (oData, response) {

			var lModel = this.getOwnerComponent().getModel("lModel");
			var cModel = this.getOwnerComponent().getModel("capiBP");

			var aDetails = lModel.getProperty("/BP_Details"),
				aPartners = lModel.getProperty("/Partners"),
				aComm = lModel.getProperty("/Communication"),
				aSales = lModel.getProperty("/SalesArea"),
				aCollections = lModel.getProperty("/Collections");

			aDetails.PartnerNumber = oData.BusinessPartner;

			var aModel = new sap.ui.model.json.JSONModel("aModel");
			aModel.setDefaultBindingMode("OneWay");

			// Partners
			var i = 0;
			for (; aPartners[i];) {
				var aHeader = {
					BPNumber: aPartners[i].PartnerNumber,
					GroupingKey: aPartners[i].CustGrouping
				};
				aModel.setProperty("/", aHeader);

				// Address
				var toAddress = {
					BPNumber: aPartners[i].PartnerNumber,
					FloorNo: aPartners[i].FloorNumber,
					BuilldingNo: aPartners[i].BuildingNumber,
					RoomNo: aPartners[i].RoomNumber,
					Country: aPartners[i].CountryKey,
					Region: aPartners[i].RegionState,
					PreferredMethod: aPartners[i].PreferredCommType
				};
				aModel.setProperty("/toAddress", toAddress);

				if (aPartners[i].PartnerType === Utility.keyBTP) {
					// Additional Attributes 
					var toBTPAddlData = [];
					var BTPAddlData = {
						BPNumber: aPartners[i].PartnerNumber,
						SalesOrg: "ZB2P",
						AdSales: aPartners[i].AdSales,
						ChannelSales: aPartners[i].ChannelSales,
						ContentSales: aPartners[i].ContentSales,
						Mode: "I"
					};
					toBTPAddlData.push(BTPAddlData);

					if (toBTPAddlData.length > 0) {
						aModel.setProperty("/toAddlData", toBTPAddlData);
					}
				}

				// Emails 
				var j = 0,
					toComm = [];
				if (aPartners[i].PartnerType === Utility.keyBTP) {
					for (; aComm[j];) {
						if (aComm[j].PartnerType === aPartners[i].PartnerType && aComm[j].PartnerSeqNum === aPartners[i].PartnerSeqNum &&
							aComm[j].CommType === Utility.keyEmail) {
							var email = {
								BPNumber: aPartners[i].PartnerNumber,
								SeqNo: aComm[j].CommSeqNumber,
								EmailAdd: aComm[j].CommValue,
								Remarks: aComm[j].CommNotes,
								Deafult: (aComm[i].DefaultComm === true) ? "X" : ""
							};
							toComm.push(email);
						}
						j++;
					}
					if (toComm.length > 0) {
						aModel.setProperty("/toComm", toComm);
					}
				}

				var aData = aModel.getProperty("/");

				// var that = this;
				var changeSetId = "BTP_A" + i;
				cModel.create("/A_Header", aData, {
					changeSetId: changeSetId,
					success: function (rData, rResponse) {
						MToast.show("Bill To Party additional data updated");
					},
					error: function (oError) {
						MToast.show("Bill To Party additional data update failed");
					}
				});
				i++;
			}

			// **********************************
			// Customer Additional info update
			var bModel = new sap.ui.model.json.JSONModel("bModel");
			bModel.setDefaultBindingMode("OneWay");

			//
			aHeader = {
				BPNumber: aDetails.PartnerNumber,
				GroupingKey: aDetails.CustGrouping
			};
			bModel.setProperty("/", aHeader);

			// Address
			toAddress = {
				BPNumber: aDetails.PartnerNumber,
				FloorNo: aDetails.FloorNumber,
				BuilldingNo: aDetails.BuildingNumber,
				RoomNo: aDetails.RoomNumber,
				Country: aDetails.CountryKey,
				Region: aDetails.RegionState,
				PreferredMethod: aDetails.PreferredCommType
			};
			bModel.setProperty("/toAddress", toAddress);

			// Emails 
			j = 0;
			toComm = [];
			for (; aComm[j];) {
				if (aComm[j].PartnerType === aDetails.PartnerType && aComm[j].PartnerSeqNum === aDetails.PartnerSeqNum &&
					aComm[j].CommType === Utility.keyEmail) {
					email = {
						BPNumber: aDetails.PartnerNumber,
						SeqNo: aComm[j].CommSeqNumber,
						EmailAdd: aComm[j].CommValue,
						Remarks: aComm[j].CommNotes,
						Deafult: (aComm[i].DefaultComm === true) ? "X" : ""
					};
					toComm.push(email);
				}
				j++;
			}
			if (toComm.length > 0) {
				bModel.setProperty("/toComm", toComm);
			}

			// 
			var toCredit = {
				BPNumber: aDetails.PartnerNumber,
				CreditLimit: aDetails.CreditLimit.toString(),
				RiskClass: aDetails.RiskClass
			};
			bModel.setProperty("/toCredit", toCredit);

			//
			var toCollections = [];
			i = 0;
			for (; aCollections[i];) {
				var Coll = {
					BPNumber: aDetails.PartnerNumber,
					CollSegment: aCollections[i].CollSegment,
					CollGroup: aCollections[i].CollGroup,
					CollSpecialist: aCollections[i].CollSpecialist,
					Mode: "I"
				};
				toCollections.push(Coll);
				i++;
			}
			bModel.setProperty("/toCollections", toCollections);
			//
			var toAddlData = [];
			i = 0;
			for (; aSales[i];) {
				var AddlData = {
					BPNumber: aDetails.PartnerNumber,
					SalesOrg: aSales[i].SalesOrg,
					AdSales: aDetails.AdSales,
					ChannelSales: aDetails.ChannelSales,
					ContentSales: aDetails.ContentSales,
					AgencyCustomer: aDetails.AgencyCustomer,
					ProformaCustomer: aDetails.ProformaCustomer,
					PaymentTerms: aDetails.PaymentTerms,
					Dunning: aDetails.DunningProcedure,
					Mode: "I"
				};

				toAddlData.push(AddlData);
				i++;
			}
			if (toAddlData.length > 0) {
				bModel.setProperty("/toAddlData", toAddlData);
			}

			//
			var bData = bModel.getProperty("/");

			var that = this;
			changeSetId = "Customer";
			cModel.create("/A_Header", bData, {
				changeSetId: changeSetId,
				success: function (rData, rResponse) {
					MToast.show("Additional Data updated");
					// Update the Cloud DB 
					that._saveRequest(Utility.actionApprove);
				},
				error: function (oError) {
					MToast.show("Additional Data update failed");
				}
			});
		},

		// 
		updateBPErrorLog: function (oError) {

			var errorMsg = JSON.parse(oError.responseText);
			var message = "BP - " + errorMsg.error.message.value;
			MToast.show(message);

			// MToast.show("Business Partner not created, please try again");
		},

		// *******************************************************************
		createBTPs: function (lModel, apiModel) {

			var aPartners = lModel.getProperty("/Partners"),
				aComm = lModel.getProperty("/Communication");

			var j = 0;

			// 
			for (; aPartners[j];) {

				if (aPartners[j].PartnerType === Utility.keyBTP) {
					var that = this;
					if (aPartners[j].PartnerNumber === "" || aPartners[j].PartnerNumber === null) {

						aPartners[j].CustGrouping = "ZBBP";
						var aDetails = aPartners[j];

						var jModel = new sap.ui.model.json.JSONModel("jModel");

						// Header info
						var vHeader = {
							BusinessPartnerCategory: "2", // Org 1-Person 
							BusinessPartnerGrouping: aDetails.CustGrouping,
							// AuthorizationGroup: "ZBBP",
							OrganizationBPName1: aDetails.CustName1,
							OrganizationBPName2: aDetails.CustName2,
							OrganizationBPName3: aDetails.CustName3,
							OrganizationBPName4: aDetails.CustName4,
							SearchTerm1: aDetails.SearchTerm1,
							FormOfAddress: "0003", // Company
							Language: "EN"
						};

						jModel.setProperty("/", vHeader);
						var vAddress = [];

						// Address
						var address = {
							HouseNumber: aDetails.HouseNumber,
							StreetName: aDetails.StreetName,
							StreetPrefixName: aDetails.Street2,
							AdditionalStreetPrefixName: aDetails.Street3,
							StreetSuffixName: aDetails.Street4,
							District: aDetails.District,
							PostalCode: aDetails.PostalCode,
							CityName: aDetails.City,
							Country: aDetails.CountryKey,
							Region: aDetails.RegionState,
							POBox: aDetails.POBoxNumber,
							Language: "EN"

							// PrfrdCommMediumType: aDetails.PreferredCommType
						};

						// Communication 
						var i = 0;

						address.to_PhoneNumber = [];
						address.to_MobilePhoneNumber = [];

						for (; aComm[i];) {
							if (aComm[i].PartnerType === Utility.keyBTP) {
								switch (aComm[i].CommType) {
								case Utility.keyTel:
									{
										var vTel = {
											PhoneNumber: aComm[i].CommValue
										};

										address.to_PhoneNumber.push(vTel);
										break;
									}
								case Utility.keyMob:
									{
										var vMob = {
											PhoneNumber: aComm[i].CommValue
										};

										address.to_MobilePhoneNumber.push(vMob);
										break;
									}
								}
							}
							i++;
						}

						vAddress.push(address);
						jModel.setProperty("/to_BusinessPartnerAddress", vAddress);
						//
						// Business Partner Role 
						var vRoles = [{
							"BusinessPartnerRole": "TR0818" // Bill To Party
						}];
						jModel.setProperty("/to_BusinessPartnerRole", vRoles);

						// Get Hierarchy Data
						var jData = jModel.getProperty("/");

						var changeSetId = "BTP" + j;
						apiModel.create("/A_BusinessPartner", jData, {
							groupId: "BTP",
							changeSetId: changeSetId,
							success: function (oData, response) {
								that._indexBTP += 1;
								that.collectBTPs(oData, response);
							},
							error: function (oError) {
								that._indexBTP += 1;
								that.collectBTPsErrors(oError);
							}
						});
					}
					// BTP already created 
					else {
						that._indexBTP += 1;
						MToast.show("Bill To Party " + aPartners[j].PartnerNumber + " already created");
						this.checkAllPartnersCreated();
					}
				}
				j++;
			}
		},

		// 
		collectBTPs: function (oData, response) {
			var lModel = this.getOwnerComponent().getModel("lModel");
			var aPartners = lModel.getProperty("/Partners");

			var i = 0;
			for (; aPartners[i];) {
				if (aPartners[i].PartnerType === Utility.keyBTP && aPartners[i].CustName1 === oData.OrganizationBPName1) {
					aPartners[i].PartnerNumber = oData.BusinessPartner;
				}
				i++;
			}
			//
			var message = "Bill To Party " + oData.BusinessPartner + " Created";
			MToast.show(message);
			//
			this.checkAllPartnersCreated();
		},
		//
		collectBTPsErrors: function (oError) {
			this.hasBTPError = true;
			var errorMsg = JSON.parse(oError.responseText);
			var message = "Bill to Party - " + errorMsg.error.message.value;
			MToast.show(message);

			this.checkAllPartnersCreated();
			// MToast.show("Bill To Party creation failed");
		},

		// *******************************************************************
		// Contact Persons 
		createCPs: function (lModel, apiModel) {
			/* */
			var aPartners = lModel.getProperty("/Partners"),
				aComm = lModel.getProperty("/Communication");

			var j = 0;

			// 
			for (; aPartners[j];) {

				if (aPartners[j].PartnerType === Utility.keyContact) {
					var that = this;
					if (aPartners[j].PartnerNumber === "" || aPartners[j].PartnerNumber === null) {

						aPartners[j].CustGrouping = "ZCON";
						var aDetails = aPartners[j];

						var jModel = new sap.ui.model.json.JSONModel("jModel");

						// Header info
						var vHeader = {
							BusinessPartnerCategory: "1", // Org 1-Person 
							BusinessPartnerGrouping: aDetails.CustGrouping,
							FirstName: aDetails.CustName1,
							LastName: aDetails.CustName2,
							FormOfAddress: aDetails.Title,
							Language: "EN"
						};

						jModel.setProperty("/", vHeader);
						var vAddress = [];

						// Address
						var address = {
							HouseNumber: aDetails.HouseNumber,
							StreetName: aDetails.StreetName,
							StreetPrefixName: aDetails.Street2,
							AdditionalStreetPrefixName: aDetails.Street3,
							StreetSuffixName: aDetails.Street4,
							District: aDetails.District,
							PostalCode: aDetails.PostalCode,
							CityName: aDetails.City,
							Country: aDetails.CountryKey,
							Region: aDetails.RegionState,
							POBox: aDetails.POBoxNumber,
							Language: "EN",

							PrfrdCommMediumType: aDetails.PreferredCommType
						};

						// Communication 
						var i = 0;

						address.to_PhoneNumber = [];
						address.to_MobilePhoneNumber = [];
						address.to_EmailAddress = [];

						for (; aComm[i];) {
							if (aComm[i].PartnerType === Utility.keyContact) {
								switch (aComm[i].CommType) {
								case Utility.keyTel:
									{
										var vTel = {
											PhoneNumber: aComm[i].CommValue
										};

										address.to_PhoneNumber.push(vTel);
										break;
									}
								case Utility.keyMob:
									{
										var vMob = {
											PhoneNumber: aComm[i].CommValue
										};

										address.to_MobilePhoneNumber.push(vMob);
										break;
									}
								case Utility.keyEmail:
									{
										var vEmail = {
											EmailAddress: aComm[i].CommValue
										};

										address.to_EmailAddress.push(vEmail);
										break;
									}
								}
							}
							i++;
						}
						vAddress.push(address);
						jModel.setProperty("/to_BusinessPartnerAddress", vAddress);

						// Get Hierarchy Data
						var jData = jModel.getProperty("/");

						var changeSetId = "CP" + j;
						apiModel.create("/A_BusinessPartner", jData, {
							groupId: "CP",
							changeSetId: changeSetId,
							success: function (oData, response) {
								that._indexCP += 1;
								that.collectCPs(oData, response);
							},
							error: function (oError) {
								that._indexCP += 1;
								that.collectCPsErrors(oError);
							}
						});
					}
					// CP already created 
					else {
						that._indexCP += 1;
						MToast.show("Contact Person " + aPartners[j].PartnerNumber + " already created");
						this.checkAllPartnersCreated();
					}
				}
				j++;
			}
		},

		// 
		// 
		collectCPs: function (oData, response) {
			var lModel = this.getOwnerComponent().getModel("lModel");
			var aPartners = lModel.getProperty("/Partners");

			var i = 0;
			for (; aPartners[i];) {
				if (aPartners[i].PartnerType === Utility.keyContact && aPartners[i].CustName1 === oData.FirstName &&
					aPartners[i].CustName2 === oData.LastName) {
					aPartners[i].PartnerNumber = oData.BusinessPartner;
				}
				i++;
			}

			var message = "Contact Person " + oData.BusinessPartner + " Created";
			MToast.show(message);

			// 
			this.checkAllPartnersCreated();
		},

		//
		collectCPsErrors: function (oError) {
			this.hasCPError = true;

			var errorMsg = JSON.parse(oError.responseText);
			var message = "Contact Person - " + errorMsg.error.message.value;
			MToast.show(message);

			this.checkAllPartnersCreated();
		},

		// check all processed requests are finished?
		checkAllPartnersCreated: function () {
			var processedPartners = this._indexBTP + this._indexCP;
			if (this.totalPartners === processedPartners) {
				if (this.hasCPError === true || this.hasBTPError === true) {
					// MToast.show("All the Bill Tos or Contact Persons are not created, Try again");
				}
				// No errors
				else {
					this.createBusinessPartner();
				}
			}
		},

		// Send email notification 
		sendEmail: function (pAction, pHeader) {

			var jModel = new sap.ui.model.json.JSONModel("jModel");
			var uiData = this.getOwnerComponent().getModel("uiModel").getProperty("/");

			// get Sales Orgs 
			var lModel = this.getOwnerComponent().getModel("lModel"),
				aSales = lModel.getProperty("/SalesArea"),
				emailModel = this.getOwnerComponent().getModel("emailModel"),
				aDetails = lModel.getProperty("/BP_Details");

			// Email Header info
			var emailHeader = {
				RequestNumber: pHeader.RequestNumber,
				BPNumber: pHeader.BPNumber,
				RequestType: Utility.reqTypeNew,
				// (pHeader.BPNumber === null || pHeader.BPNumber === undefined) ? Utility.reqTypeNew : Utility.reqTypeUpdate,
				/*																						 N - New, U - Change */
				EmailFromRole: Utility._userRole,
				CurrentUser: Utility.userModel.getProperty("/").name.toUpperCase(),
				Requestor: pHeader.CreatedBy,
				UserAction: pAction,
				CountryKey: pHeader.CountryKey,
				AdSales: uiData.AdSales,
				ContentSales: uiData.ContentSales,
				BrandedServices: uiData.ChannelSales,
				Comments: aDetails.Comments,
				ContractValue: aDetails.ContractValue,
				CreditLimit: aDetails.CreditLimit,
				ContractCurrency: aDetails.ContractCurrency,
				CreatedDate: pHeader.CreatedTstamp.toString(),
				GroupingKey: aDetails.CustGrouping
			};

			jModel.setProperty("/", emailHeader);

			var toSalesA = [];
			var i = 0;
			for (; aSales[i];) {
				var toSales = {
					RequestNumber: pHeader.RequestNumber,
					SalesOrg: aSales[i].SalesOrg
				};
				toSalesA.push(toSales);
				i++;
			}
			jModel.setProperty("/toSalesOrgs", toSalesA);

			// Create 
			var jData = jModel.getProperty("/");
			// var that = this;
			emailModel.create("/A_EmailHeader", jData, {
				success: function (oData, response) {
					MToast.show("Email sent :)");
				},
				error: function (oError) {
					MToast.show("Email failed :(");
				}
			});
		}
	});
});